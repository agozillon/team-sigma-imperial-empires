#ifndef UNIT_H
#define UNIT_H
#include "WorldObjects.h"
#include "BoundingBox.h"
#include <string>
using namespace glm;	

class Unit : public WorldObjects
{
public:
	const inline vec3 getPosition(){return position;}
	const inline vec3 getRotation(){return rotation;}
	const inline vec3 getScalar(){return scalar;}
	const inline void updatePath(std::vector<vec3> nPath){currentPath = nPath;}
	void draw(mat4 viewProjection);
	inline void updatePosition(vec3 pos){position = pos;}
	inline void updateRotation(vec3 rot){rotation = rot;}
	inline void updateScalar(vec3 scale){scalar = scale;}
	inline void updateSlowEffect(bool nValue){slowEffect = nValue;}
	inline void setTarget(Unit * nTarget){target = nTarget;} 
	void traversePath();
	inline BoundingBox * getCollisionBox(){return collisionBox;}
	inline BoundingBox * getRangeBox(){return rangeBox;}
	void calculateCombat(Unit* attacker);
	bool operator > (const Unit* param);
	bool operator < (const Unit* param);
	bool operator == (const Unit* param);
	~Unit();
	Unit(std::string type, std::vector<std::string> weaknesses, std::vector<std::string> strengths, vec3 pos, vec3 rot, vec3 scale, 
	vec3 rng, int arm, int hp, int dmg, float sp, int unitCount, int unitsInRow, std::string mesh, std::string texture, std::string shader);

protected:
	std::vector<std::string> unitWeaknesses;
	std::vector<std::string> unitStrengths;
	std::string unitType;

private:
	vec3 scalar;
	vec3 rotation;
	vec3 position;
	vec3 range;
	vec3 soldiersDimensions;
	int armor;
	int health;
	int damage;
	float speed;
	int numberOfTroops;
	int unitsPerRow;
	bool slowEffect;
	BoundingBox * rangeBox;
	BoundingBox * collisionBox;
	Unit * target;
	std::vector<vec3> currentPath;
	std::string meshName;
	std::string textureName;
	std::string shaderName;
	
};

#endif