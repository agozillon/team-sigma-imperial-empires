#ifndef LEVELMANAGER_H
#define LEVELMANAGER_H
#include "Level.h"
#include <map>
#include <vector>
#include <string>

class Scenery;

class LevelManager{
public:
	~LevelManager();
	LevelManager();
	void init();
	void draw();
	void switchLevel(std::string levelName);
	void update();
	void getScenery(std::vector<Scenery*> &scenery);

private:
	void getLevel(std::string levelName);

	Level * currentLevel;
	std::map<std::string, Level*> levelList;

};
#endif