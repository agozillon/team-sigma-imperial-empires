#include "RenderManager.h"

#include "Game.h"
RenderManager * RenderManager::instance;

void RenderManager::createInstance()
{	
	instance = new RenderManager();
}

RenderManager * RenderManager::getInstance()
{
	return instance;
}

void RenderManager::deleteInstance()
{
	delete instance;
	instance = NULL;
}



RenderManager::RenderManager()
{
	projection = glm::perspective(60.0f,(float)GameManager::getInstance()->getWidth()/(float)GameManager::getInstance()->getHeight(),1.0f, 400.0f);
}

bool RenderManager::contains(const std::string name)
{
	return renderables.find(name) != renderables.end();
}

RenderManager::~RenderManager()
{

}


void RenderManager::addRenderable(const std::string name, Renderable * r)
{
	if( renderables.find(name) != renderables.end() )
	{
		std::cout << "Error renderable " << name << "already in list" << std::endl;
		return;
	}
	renderables[name] = r;
	renderables[name]->setName(name);

}


// cast a renderable object to a mesh for access to mesh data
// MUST BE A MESH has no type safety, so if you call it on a sprite
// it will crash. Should never be dynamically called at run time 
// to avoid this
const Mesh * RenderManager::getRenderableToMesh(const std::string n) const
{
	return (Mesh*)renderables.find(n)->second;
}

void RenderManager::renderRenderable(const std::string name)
{

	if( renderables.find(name) == renderables.end() ) 
	{
		std::cout << "Error renderable " << name << " not found in list of renderables" << std::endl;
		return; //if renderabel has not been added
	}
	renderables[name]->render();
}

void RenderManager::loadMesh(const std::string &n,const std::string &fp)
{
	if( renderables.find(n) != renderables.end() )
	{
		std::cout << "Renderable named: " << n << " already loaded " << std::endl;
		return;
	}


	ModelLoader * ml = ModelLoader::getLoader(OBJ);

	if( ml == NULL)
	{
		std::cout << "Error (probally fatal) with model loader, returned NULL" << std::endl;
		return;
	}

	renderables[n] = ml->load(fp);
	renderables[n]->setName(n);
	delete ml;
}

