#ifndef UNIT_DATA_H
#define UNIT_DATA_H

#include <map>
#include <string>
#include "Attributes.h"


class UnitData
{
public:
	UnitData();
	UnitData(const std::string & name, const std::map<std::string,Attribute> attributes);


	static const UnitData& getUnit(const std::string &name);
	static void load(const std::string &filePath);

	const Attribute & getAttribute(const std::string & name) const;




private:
	std::map<std::string,Attribute> attributes;
	std::string name;
	
	static std::map<std::string,UnitData> units;

};


#endif