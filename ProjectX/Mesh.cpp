#include "Mesh.h"



Mesh::Mesh(float *verts,const unsigned int size,float *uv,const unsigned int uvSize,
		unsigned short *indices,const unsigned int indiceSize, float * norms,const unsigned int normalSize)
		: count(size),
		indiceCount(indiceSize) 
{
	 this->indices = this->uvs = this->normals = false;
	 
	vertData = new float[size];
	for(int i = 0; i < size; i++)
	{
		vertData[i] = verts[i];
	}


	glGenVertexArrays(1,&VAO);
	glGenBuffers(1,&vertexVBO);

	glBindVertexArray(VAO);

	glBindBuffer(GL_ARRAY_BUFFER,vertexVBO);
	glBufferData(GL_ARRAY_BUFFER,sizeof(float)*size,verts,GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0,3,GL_FLOAT,GL_FALSE,0,NULL);

	
	if( uv != NULL && uvSize != 0)
	{
		glGenBuffers(1,&uvVBO);
		glBindBuffer(GL_ARRAY_BUFFER,uvVBO);
		glBufferData(GL_ARRAY_BUFFER,sizeof(float)*uvSize,uv,GL_STATIC_DRAW);
		glEnableVertexAttribArray(1);
		glVertexAttribPointer(1,2,GL_FLOAT,GL_FALSE,0,NULL);

		uvs = true;
	}

	if( indiceSize != 0 && indices != NULL)
	{
		this->indices = true; //we are using indexed drawing
		glGenBuffers(1,&indiceVBO);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,indiceVBO);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER,sizeof(unsigned short)*indiceCount,indices,GL_STATIC_DRAW);
		this->indices = true;
	}

	if( normalSize != 0 && norms != NULL)
	{
		this->normals = true;
		glGenBuffers(1,&normalVBO);
		glBindBuffer(GL_ARRAY_BUFFER,normalVBO);
		glBufferData(GL_ARRAY_BUFFER,sizeof(float)*normalSize,norms,GL_STATIC_DRAW);
		glEnableVertexAttribArray(2);
		glVertexAttribPointer(2,3,GL_FLOAT,GL_FALSE,0,NULL);

	}

}


Mesh::~Mesh()
{
	glDeleteBuffers(1,&vertexVBO);
	glDeleteVertexArrays(1,&VAO);

	if( uvs ) glDeleteBuffers(1,&uvVBO);
	if( indices ) glDeleteBuffers(1,&indiceVBO);
	if( normals ) glDeleteBuffers(1,&normalVBO);
}

void Mesh::render() const
{
	if( !indices )
	{
		draw();
	} 
		else 
	{
		indexDraw();
	}

}


void Mesh::indexDraw() const
{
	glBindVertexArray(VAO);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,indiceVBO);
	glDrawElements(GL_TRIANGLES,indiceCount,GL_UNSIGNED_SHORT,0);
}

void Mesh::draw() const
{
	glBindVertexArray(VAO);
	glBindBuffer(GL_ARRAY_BUFFER,vertexVBO);
	glDrawArrays(GL_TRIANGLES,0,count);
}