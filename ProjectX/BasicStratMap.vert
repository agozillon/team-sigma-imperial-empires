#version 330

layout(location = 0) in vec3 in_verts;
layout(location = 1) in vec2 in_UV;
layout(location = 2) in vec3 in_norms;

uniform mat4 MVP;
uniform mat4 projection;
out vec2 UV;

void main()
{
	vec4 pos =  MVP*vec4(in_verts,1.0);
	UV = in_UV;

	gl_Position = projection*pos;
	
}
