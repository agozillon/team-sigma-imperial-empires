#include "Map.h"
#include "RenderManager.h"
#include "MapManager.h"
#include "CameraManager.h"
#include "Mouse.h"
#include "GameManager.h"



Map::Map(std::vector<std::vector<Tile>>*tiles)
{
	map = tiles;
	width = tiles->size();
	height = (*tiles)[0].size();
}


int Map::getHeight()
{
	return height;
}

int Map::getWidth()
{
	return width;
}

void Map::setDimentions(int w,int h)
{
	width = w;
	height = h;
}

void Map::draw()
{
	ShaderManager::getInstance()->use("StrategicMap");

	ShaderManager::getInstance()->getShader("StrategicMap")->setUniformMatrix4fv("projection",1,false,RenderManager::getInstance()->getProjectionValuePtr());

	

	for(int y =height-1;y >= 0; y--)
	{
		for(int x = 0;x != width; x++)
		{	
			glm::mat4 cam = CameraManager::getInstance()->lookAt();
			glm::mat4 mvp(1.0f);

			mvp = glm::translate(mvp,glm::vec3(x,y,0.0f));
			mvp = glm::translate(mvp,-(CameraManager::getInstance()->getEye()));

			 
			ShaderManager::getInstance()->getShader("StrategicMap")->setUniformMatrix4fv("MVP",1,false,glm::value_ptr(mvp));

			MapManager::getInstance()->draw(&(*map)[x][y],mvp);
		}
	}
}

const std::vector<std::vector<Tile>> * Map::getMap()
{
	return map;
}


void Map::checkMouseCollision()
{

	float x = (((float)Mouse::getInstance()->getX()/ ((float)GameManager::getInstance()->getWidth()*0.5f)) -1.0f);  // (x/(width/2)) - 1
	float y = (((float) Mouse::getInstance()->getY()/ ((float) (GameManager::getInstance()->getHeight()*0.5))) -1.0f);// (y/(height/2)) -1


	int tileSpaceX = (((x+1)/2) * -CameraManager::getInstance()->getEye().x);
	int tileSpaceY = (((y+1)/2) * -CameraManager::getInstance()->getEye().y);

	//std::cout << (x+1)/2 << "  " << (y+1)/2 << "  " << tileSpaceX << "  " << tileSpaceY << std::endl;
	//if( Mouse::getInstance()->getLeftState() == BUTTON_STATE_DOWN )
	//	std::cout << (*map)[x][y].getOwner() << std::endl;

	
}

/*
Tile & Map::getTile(float x,float y)
{
	return map[0];
}*/