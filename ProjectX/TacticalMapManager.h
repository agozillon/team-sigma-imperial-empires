#ifndef TACTICAL_MAP_MANAGER_H
#define TACTICAL_MAP_MANAGER_H
#include "LevelManager.h"
#include "Unit.h"

class TacticalMapManager
{
public:

	static void createInstance();
	static TacticalMapManager * getInstance();
	static void deleteInstance();

	void draw();
	void update();
	void checkKeyPress();

private:

	static TacticalMapManager * instance;
	TacticalMapManager();
	~TacticalMapManager();

	Unit * test;
	Unit * test2;
	LevelManager * levelManager;

};
#endif