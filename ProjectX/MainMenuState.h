#ifndef MAIN_MENU_STATE_H
#define MAIN_MENU_STATE_H
#include "GameState.h"
#include "WidgetManager.h"

class MainMenuState: public GameState
{
public:
	MainMenuState();
	~MainMenuState();

	void draw();
	void update();
	void onEntry(){ }
	void onExit() { }

private:

	void checkInput();
	WidgetManager widgetManager;
};

#endif