#ifndef WIDGET_EVENT_HANDLER_H
#define WIDGET_EVENT_HANDLER_H

class Widget;


//The base event handler
//When a widget is interacted with it will call the relevent method 
class WidgetEventHandler
{
public:
	//Called when the mouse hovers above the widget
	virtual void handleMouseOver(Widget *) { }

	//Called when the widget is left clicked 
	virtual void handleMouseLeftClick(Widget *) { }
	
	//Called when the widget is right clicked
	virtual void handleMouseRightClick(Widget *) { }

	//Called when the widget is dragged
	virtual void handleDragged(Widget *) { }

	//Handles when the widget is docked
	virtual void handleDocked(Widget *) { }

	//Handles when the widget is undocked
	virtual void handleUndock(Widget *) { }
protected:
	WidgetEventHandler() { } 
};


#endif