#include "ForestLevel.h"
#include "RenderManager.h"
#include "CameraManager.h"
#include "Mouse.h"

ForestLevel::ForestLevel()
{	

	// log
	sceneryList.push_back(new Scenery(glm::vec3(5, 0, -4.5), glm::vec3(45, 0, 0), glm::vec3(1.0, 1.0, 1.0), "Log", "Log", "simpleTac"));
	sceneryList.push_back(new Scenery(glm::vec3(10, -10, -5), glm::vec3(90, 45, 0), glm::vec3(1.0, 1.0, 1.0), "Log", "Log", "simpleTac"));
	sceneryList.push_back(new Scenery(glm::vec3(-7, -7, -5), glm::vec3(90, -45, 0), glm::vec3(1.0, 1.0, 1.0), "Log", "Log", "simpleTac"));
	sceneryList.push_back(new Scenery(glm::vec3(-18, -16, -5), glm::vec3(90, 0, 0), glm::vec3(1.0, 1.0, 1.0), "Log", "Log", "simpleTac"));
	sceneryList.push_back(new Scenery(glm::vec3(-18, 2, -5), glm::vec3(90, 25, 0), glm::vec3(1.0, 1.0, 1.0), "Log", "Log", "simpleTac"));
	
	// tiled floor, unforutnately can't just repeat draw them otherwise picking wouldn't work
	// maybe making a texture(couldn't source one) that can stretch across a huge cube
	// and not look like one patch of grass would be more optimal
	for(int x = -2; x < 2; x++)
	{
		for(int y = -2; y < 2; y++)
		{
			sceneryList.push_back(new Scenery(glm::vec3(x*10, y*10, -5), glm::vec3(0, 0, 0), glm::vec3(5.0, 5.0, 0.02), "Cube", "RealGrass", "simpleTac"));
		}
	}

	// arbol trees
	sceneryList.push_back(new Scenery(glm::vec3(-12, 14, -5), glm::vec3(90, 0, 0), glm::vec3(1.0, 1.0, 1.0), "Tree1", "Tree1", "simpleTac"));
	sceneryList.push_back(new Scenery(glm::vec3(-11, 11, -5), glm::vec3(90, 0, 0), glm::vec3(1.0, 1.0, 1.0), "Tree1", "Tree1", "simpleTac"));
	sceneryList.push_back(new Scenery(glm::vec3(-12, 5, -5), glm::vec3(90, 0, 0), glm::vec3(1.0, 1.0, 1.0), "Tree1", "Tree1", "simpleTac"));
	sceneryList.push_back(new Scenery(glm::vec3(-2, 8, -5), glm::vec3(90, 0, 0), glm::vec3(1.0, 1.0, 1.0), "Tree1", "Tree1", "simpleTac"));
	sceneryList.push_back(new Scenery(glm::vec3(-10, -15, -5), glm::vec3(90, 0, 0), glm::vec3(1.0, 1.0, 1.0), "Tree1", "Tree1", "simpleTac"));
	sceneryList.push_back(new Scenery(glm::vec3(4, -14, -5), glm::vec3(90, 0, 0), glm::vec3(1.0, 1.0, 1.0), "Tree1", "Tree1", "simpleTac"));

	// Pino Pine tree
	sceneryList.push_back(new Scenery(glm::vec3(-8, 12, -5), glm::vec3(90, 0, 0), glm::vec3(1.0, 1.0, 1.0), "Tree2", "Tree2", "simpleTac"));
	sceneryList.push_back(new Scenery(glm::vec3(-6, 8, -5), glm::vec3(90, 0, 0), glm::vec3(1.0, 1.0, 1.0), "Tree2", "Tree2", "simpleTac"));
	sceneryList.push_back(new Scenery(glm::vec3(-8, 2, -5), glm::vec3(90, 0, 0), glm::vec3(1.0, 1.0, 1.0), "Tree2", "Tree2", "simpleTac"));
	sceneryList.push_back(new Scenery(glm::vec3(-18, 12, -5), glm::vec3(90, 0, 0), glm::vec3(1.0, 1.0, 1.0), "Tree2", "Tree2", "simpleTac"));
	sceneryList.push_back(new Scenery(glm::vec3(-4, -22, -5), glm::vec3(90, 0, 0), glm::vec3(1.0, 1.0, 1.0), "Tree2", "Tree2", "simpleTac"));

	// Pine tree
	sceneryList.push_back(new Scenery(glm::vec3(-15, 10, -5), glm::vec3(90, 0, 0), glm::vec3(0.02, 0.02, 0.02), "Tree3", "Tree3", "simpleTac"));
	sceneryList.push_back(new Scenery(glm::vec3(-4, 4, -5), glm::vec3(90, 0, 0), glm::vec3(0.02, 0.02, 0.02), "Tree3", "Tree3", "simpleTac"));
	sceneryList.push_back(new Scenery(glm::vec3(-4, -16, -5), glm::vec3(90, 0, 0), glm::vec3(0.02, 0.02, 0.02), "Tree3", "Tree3", "simpleTac"));
	sceneryList.push_back(new Scenery(glm::vec3(4, -20, -5), glm::vec3(90, 0, 0), glm::vec3(0.02, 0.02, 0.02), "Tree3", "Tree3", "simpleTac"));

	// huge tree
	sceneryList.push_back(new Scenery(glm::vec3(-10, 8, -5), glm::vec3(90, 0, 0), glm::vec3(0.002, 0.002, 0.002), "Tree4", "Tree4", "simpleTac"));
	sceneryList.push_back(new Scenery(glm::vec3(-4, 14, -5), glm::vec3(90, 0, 0), glm::vec3(0.002, 0.002, 0.002), "Tree4", "Tree4", "simpleTac"));
	sceneryList.push_back(new Scenery(glm::vec3(-4, -10, -5), glm::vec3(90, 0, 0), glm::vec3(0.002, 0.002, 0.002), "Tree4", "Tree4", "simpleTac"));
	sceneryList.push_back(new Scenery(glm::vec3(-12, -22, -5), glm::vec3(90, 0, 0), glm::vec3(0.002, 0.002, 0.002), "Tree4", "Tree4", "simpleTac"));
}

ForestLevel::~ForestLevel()
{

}

void ForestLevel::init()
{

}

void ForestLevel::draw()
{

	ShaderManager::getInstance()->use("simpleTac"); //find that shader in list, and use it
	glm::mat4 projection = RenderManager::getInstance()->getProjection(); // usual GLM stuff
	glm::mat4 view = CameraManager::getInstance()->lookAt();
	glm::mat4 viewProjection = projection * view;

	for(int i = 0; i < sceneryList.size(); i++)
	{
		sceneryList[i]->draw(viewProjection);
	}
}
	
void ForestLevel::update()
{

}
	
void ForestLevel::getScenery(std::vector<Scenery*> &scenery)
{
	scenery = sceneryList;
}

