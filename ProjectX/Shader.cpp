#include "Shader.h"
#include <fstream>
#include <iostream>

Shader::~Shader()
{
	glDeleteShader(handle);
}

Shader::Shader(std::string name,ShaderType t)
{

	if( t == VERTEX )
		handle = glCreateShader(GL_VERTEX_SHADER);
	else if( t == FRAGMENT )
		handle = glCreateShader(GL_FRAGMENT_SHADER);
	
	shaderName = name;
	type = t;
	loaded = compiled = false;

	GLenum err = glGetError();
	if( err != GL_NO_ERROR )
	{
		std::cout << "Shader: " << shaderName << " encountered error: " << err << " whilst creating shader" << std::endl;
		int length;
		glGetShaderiv(handle,GL_INFO_LOG_LENGTH,&length);

		char * ch = new char[length];
		glGetShaderInfoLog(handle,length,NULL,ch);
		std::cout << ch << std::endl;
		return;

	}
}

void Shader::load(std::string filePath)
{

	if(loaded ) return;
	std::ifstream source(filePath.c_str(), std::ios::binary | std::ios::ate| std::ios::in);
	
	GLuint size = source.tellg();
	source.seekg(source.beg);

	char * str = new char[size+1];
	const char * block = str;

	source.read(str,size);
	source.close();
	str[size] = '\0';

	glShaderSource(handle,1,&block,NULL);

	GLenum err = glGetError();
	if( err != GL_NO_ERROR )
	{
		std::cout << "Shader: " << shaderName << " encountered error: " << err << " whilst loading source" << std::endl;
		int length;
		glGetShaderiv(handle,GL_INFO_LOG_LENGTH,&length);

		char * ch = new char[length];
		glGetShaderInfoLog(handle,length,NULL,ch);
		std::cout << ch << std::endl;
		
		loaded=false;
		return;
	}


	loaded = true;
	delete[] str;
}


void Shader::compile()
{
	if( compiled ) return;
	glCompileShader(handle);


	int comp;
	glGetShaderiv(handle,GL_COMPILE_STATUS,&comp);
	GLenum err = glGetError();
	if( err != GL_NO_ERROR || !comp )
	{
		std::cout << "Shader: " << shaderName << " encountered error: " << err << " whilst compiling source" << std::endl;
		int length;
		glGetShaderiv(handle,GL_INFO_LOG_LENGTH,&length);

		char * ch = new char[length];
		glGetShaderInfoLog(handle,length,NULL,ch);
		std::cout << ch << std::endl;
		compiled = false;
		return;
	}
	compiled = true;
}
