#ifndef WIDGET_MANAGER_H
#define WIDGET_MANAGER_H
#include "Widget.h"
#include <map>

/*
	*Not singleton* 
	Intended for each state that will require GUI elements to create it's own instance of this class
	passing in the GUI elements it will use 
*/
class WidgetManager: public Widget
{
public:
	WidgetManager();
	~WidgetManager() { }

	bool checkMouse();

	//inline const Widget * getWidget(const std::string & str) const { return children.find(str)->second; }
	inline const Widget * operator[](const std::string &str) const { return children.find(str)->second; }
	void draw() const;

private:
	Widget *activeWidget;
	WidgetEventHandler * eventHandler;
	

};

#endif