#ifndef RENDERMANAGER_H
#define RENDERMANAGER_H

#include "ModelLoader.h"
#include "TextureManager.h"
#include "ShaderManager.h"
#include "Renderable.h"

#include <glm\glm.hpp>
#include <glm\gtc\matrix_transform.hpp>
#include <glm\gtc\type_ptr.hpp>
/*
	This class is a singleton that handles the the other rendering realted managers and maintains the list of renderables
*/

// defining mesh instead of including it, causes irritating issues with getRenderableToMesh otherwise
class Mesh;

class RenderManager
{
public:
	
	

	//inits a vertex and fragment shader

	static void createInstance(); 
	static RenderManager * getInstance();
	static void deleteInstance();
		
	bool contains(const std::string name);

	void addRenderable(const std::string n, Renderable* r);
	void renderRenderable(const std::string n);

	inline const Renderable * getRenderable(const std::string & str) const { return renderables.find(str)->second; }
	
	// returns a renderable casted to a mesh for access to its values
	// be careful you don't cast a non-mesh object or you will get runtime
	// crashes as it's not type checked etc!
	const Mesh * getRenderableToMesh(const std::string n) const;


	float* getProjectionValuePtr() {return glm::value_ptr(projection); }
	const glm::mat4 & getProjection() { return projection; }
	
	void loadMesh(const std::string & name,const std::string &filePath); //loads a model in from a 3d model file 
private:

	static RenderManager * instance;
	glm::mat4 projection;
	std::map<std::string, const Renderable*> renderables;
	~RenderManager();
	RenderManager();
};

#endif