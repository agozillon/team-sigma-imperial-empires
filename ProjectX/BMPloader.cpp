#include "BMPLoader.h"
#include <fstream>
#include <SDL.h>


BMPLoader::BMPLoader()
{

}

void * BMPLoader::invert(void * old)
{
	return NULL;
}

// Dr dan's code from RT3D, needs to be replaced
Texture BMPLoader::load(const std::string filePath)
{
	GLuint handle;
	const char * fname = filePath.c_str();

	glGenTextures(1, &handle); // generate texture ID
	// load file - using core SDL library
	SDL_Surface *tmpSurface = SDL_LoadBMP(fname);
	

	

	if (!tmpSurface)
	{
		std::cout << "Error loading bitmap" << std::endl;
	}
	// bind texture and set parameters
	glBindTexture(GL_TEXTURE_2D, handle);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

	SDL_PixelFormat *format = tmpSurface->format;
	GLuint externalFormat, internalFormat;
	if (format->Amask) {
		internalFormat = GL_RGBA;
		externalFormat = (format->Rmask < format-> Bmask) ? GL_RGBA : GL_BGRA;
	}
	else {
		internalFormat = GL_RGB;
		externalFormat = (format->Rmask < format-> Bmask) ? GL_RGB : GL_BGR;
	}




	glTexImage2D(GL_TEXTURE_2D,0,internalFormat,tmpSurface->w, tmpSurface->h, 0,
	externalFormat, GL_UNSIGNED_BYTE, tmpSurface->pixels);
	glGenerateMipmap(GL_TEXTURE_2D);


	int width = tmpSurface->w,height = tmpSurface->h;
	SDL_FreeSurface(tmpSurface); // texture loaded, free the temporary buffer
	
	return Texture(handle,width,height);
}
