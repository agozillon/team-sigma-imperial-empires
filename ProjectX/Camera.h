#ifndef CAMERA_H
#define CAMERA_H
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>


class Camera
{
public:
	Camera(const glm::vec3 &eye,const glm::vec3 &target,const glm::vec3&up);
	~Camera();

	glm::mat4 lookAt();

	void rotateCamera(const float y, const float p);
	void setEye(const glm::vec3& eye);
	void setUp(const glm::vec3& up);
	void setTarget(const glm::vec3& target);
	void moveCameraRight(float dist);
	void moveCameraUp(float dist);
	void moveCameraForward(float dist);

	const glm::vec3 & getEye(){ return eye; }
	const glm::vec3 & getTarget() { return target; }
	const glm::vec3 & getUp() { return up; }


private:
	glm::vec3 eye,target,up;
	float yaw, pitch;

	Camera(){}
};


#endif