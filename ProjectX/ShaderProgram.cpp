#include "ShaderProgram.h"
#include <iostream>



ShaderProgram::ShaderProgram(std::string name)
{
	if( name.compare("NULL") != 0)
		id = glCreateProgram();
	else id = 0; // using id = 0 for NULL


	programName = name;
	active = linked = false;

	GLenum err = glGetError();
	if( err != GL_NO_ERROR )
		std::cout << "Shader program: " << name << " encountered error: " << err << " whilst creating shader program" << std::endl;
	

}


ShaderProgram::~ShaderProgram()
{
	glDeleteProgram(id);
}




void ShaderProgram::attachShader(Shader * shader)
{
	shaders.push_back(shader);
	glAttachShader(id,shader->getHandle());

}



GLuint ShaderProgram::getID()
{
	return id;
}




void ShaderProgram::use()
{
	if( active ) return;
	glUseProgram(id);
	active = true;
}



void ShaderProgram::link()
{
	glLinkProgram(id);



	GLenum err = glGetError(); // some error checking

	if( err != GL_NO_ERROR )
	{
		
		std:: cout << "Problem linking:\n " << err << std::endl;
		int length;
		glGetShaderiv(id,GL_INFO_LOG_LENGTH,&length);

		char * ch = new char[length];
		glGetShaderInfoLog(id,length,NULL,ch);
		std::cout << ch << std::endl;
		linked = false; 
		return;
	}

	linked = true;
}



GLuint ShaderProgram::getUniformLocation(const std::string & paramName) const 
{
	GLuint loc = glGetUniformLocation(id,paramName.c_str());

	if( loc == -1)
		std::cout << "No uniform found with name: " << paramName << std::endl;

	return loc;
}



void ShaderProgram::setUniform1i(const std::string & paramName,const int i) const
{
	glUniform1i(getUniformLocation(paramName),i);
}

void ShaderProgram::setUniform1f(const std::string & str,float f) const
{
	glUniform1f(getUniformLocation(str),f);
}

void ShaderProgram::setUniform2fv(const std::string & str,int count,float *f) const
{
	glUniform2fv(getUniformLocation(str.c_str()),count,f);
}

void ShaderProgram::setUniform3fv(const std::string &str,int count,float *f) const
{
	glUniform3fv(getUniformLocation(str.c_str()),count,f);
}

void ShaderProgram::setUniform4fv(const std::string &str,int count,float *f) const
{
	glUniform4fv(getUniformLocation(str.c_str()),count,f);
}

void ShaderProgram::setUniformMatrix3fv(const std::string &str,int count,bool t,float * value) const
{
	glUniformMatrix4fv(getUniformLocation(str.c_str()),count,t,value);
}

void ShaderProgram::setUniformMatrix4fv(const std::string &str,int count,bool t,float * value) const
{
	glUniformMatrix4fv(getUniformLocation(str.c_str()),count,t,value);
}
