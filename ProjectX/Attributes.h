#ifndef ATTRIBUTES_H
#define ATTRIBUTES_H
#include <string>
#include <sstream>


enum DataType
{
	FLOAT,
	STRING,
	INTEGER,
	BOOL
};

class Attribute
{
public:
	Attribute() { }
	Attribute(const std::string & data,DataType t);

	const float getFloatData() const       {return floatData; }
	const int getIntegerData() const   {return integerData; }
	const std::string &getString() const {return stringData;}
	bool getBooleanData() const { return !!integerData;}
	const DataType getType() const { return type; }
private:

	DataType type;
	int integerData;
	float floatData;
	std::string stringData;
};

#endif