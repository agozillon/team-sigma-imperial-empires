#ifndef TACTICAL_MAP_STATE_H
#define TACTICAL_MAP_STATE_H
#include "GameState.h"
#include "TacticalMapManager.h"

class TacticalMapState: public GameState
{
public:
	TacticalMapState();
	~TacticalMapState();

	void draw();
	void update();
	void onExit() { }
	void onEntry();

private:

	void checkInput();
};
#endif