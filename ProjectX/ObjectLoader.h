#ifndef OBJ_LOADER_H
#define OBJ_LOADER_H
#include "ModelLoader.h"
#include <vector>
#include <string>
#include "Mesh.h"

// V = vertex, T = texture coordinate, N = Normal 
enum FaceFormat{
	V_ONLY,
	VT,
	VTN,
	VN,
	FORMAT_NULL
};


class ObjectLoader: public ModelLoader
{
public:

	ObjectLoader();
	~ObjectLoader();

	Renderable * load(const std::string &filePath);

private:
	std::vector<float>& generateNormals(const std::vector<float> &verts);
	void removeSlashes(std::string &);

	//returns what format the faces are in
	FaceFormat handleFace(const std::string &);

};


#endif