#include "ModelLoader.h"
#include "ObjLoader.h"


ModelLoader * ModelLoader::getLoader(ModelFormat mf)
{
	if( mf == OBJ ) 
		return new ObjectLoader();

	std::cout << "Error unknown model format" << std::endl;

	return NULL;
}