#include "WindowWidget.h"
#include "GameManager.h"
#include "Button.h"


WindowWidget::WindowWidget(const std::string &name,const Sprite *sprite, float ix,const float iy,unsigned int f,const unsigned int winflags):
	Widget(name,ix,iy,
	(float)TextureManager::getInstance()->getWidthScreensapce(sprite->getTexture()), //convert width into screenspace coords
	(float)TextureManager::getInstance()->getHeightScreensapce(sprite->getTexture())//convert height into screenspace coords
	,f),
	backdropSprite(sprite->getName()),
	windowFlags(winflags)
{

	eventHandler = new WindowEventHandler();

	float buttonX = 0.0f,buttonY = 0.0f;

	if( windowFlags & WINDOW_WIDGET_CLOSABLE )
	{
		buttonX = width - 0.01f- (TextureManager::getInstance()->getWidthScreensapce(((const Sprite*)RenderManager::getInstance()->getRenderable("WindowQuitDefault"))->getTexture())*0.5f );
		buttonY = height - 0.01f - (TextureManager::getInstance()->getHeightScreensapce(((const Sprite*)RenderManager::getInstance()->getRenderable("WindowQuitDefault"))->getTexture())*0.5f );

		children["Quit"] = new Button("Quit",(const Sprite*)RenderManager::getInstance()->getRenderable("WindowQuitDefault"),buttonX,buttonY,WIDGET_NULL,BUTTON_SHADER_TINT);
		children["Quit"]->setParent(this);
		children["Quit"]->addEventHandler(eventHandler);
	}

	if( windowFlags & WINDOW_WIDGET_DOCKABLE )
	{
		buttonX = width - 0.06f - (0.05f*(!!buttonX)) - (TextureManager::getInstance()->getWidthScreensapce(((const Sprite*)RenderManager::getInstance()->getRenderable("WindowDockDefault"))->getTexture())*0.5f );
		buttonY = height - 0.01f - (TextureManager::getInstance()->getHeightScreensapce(((const Sprite*)RenderManager::getInstance()->getRenderable("WindowDockDefault"))->getTexture())*0.5f );

		children["Dock"] = new Button("Dock",(const Sprite*)RenderManager::getInstance()->getRenderable("WindowDockDefault"),buttonX,buttonY,WIDGET_NULL,BUTTON_SHADER_TINT);

		children["Dock"]->setParent(this);
		children["Dock"]->addEventHandler(eventHandler);
	}
	

}

void WindowWidget::draw() const 
{
	glm::mat4 mvp(1.0f);

	mvp = glm::translate(mvp,glm::vec3(x,y,0));
	mvp = glm::scale(mvp,glm::vec3(width,height,0));


	glDepthMask(GL_FALSE);

	ShaderManager::getInstance()->use("GUI");
	ShaderManager::getInstance()->getShader("GUI")->setUniformMatrix4fv("MVP",1,false,glm::value_ptr(mvp));
	ShaderManager::getInstance()->getShader("GUI")->setUniform1f("tint",1.0f);

	RenderManager::getInstance()->renderRenderable(backdropSprite);


	glDepthMask(GL_TRUE);


	for(std::map<std::string,Widget*>::const_iterator itr = children.cbegin(); itr != children.cend(); itr++)
	{
		itr->second->draw();
	}
}


void WindowEventHandler::handleMouseLeftClick(Widget * widget)
{
	if( widget->getName().compare("Quit") == 0)
	{
		if( widget->getStatus() & STATUS_LEFT_CLICKED )
			widget->getParent()->getParent()->queueRemoveWidget(widget->getParent()->getName());
	}

	if( widget->getName().compare("Dock") == 0)
	{
		if( widget->getStatus() & STATUS_LEFT_CLICKED )
		{
			if( widget->getTimeStamp() < widget->getPreviousTimeStamp() + 50 ) return;

			if( !(widget->getParent()->getStatus() & STATUS_DOCKED )  )
			{
				widget->getParent()->setStatus( widget->getParent()->getStatus() | STATUS_DOCKED );
				if( widget->getParent()->getEventHandler() ) widget->getParent()->getEventHandler()->handleDocked(widget);
				((Button*)widget)->setSprite("WindowUndockDefault");
			} 
				else 
			{
				widget->getParent()->setStatus( widget->getParent()->getStatus() ^ STATUS_DOCKED );
				if( widget->getParent()->getEventHandler() ) widget->getParent()->getEventHandler()->handleUndock(widget);
				((Button*)widget)->setSprite("WindowDockDefault");

			}
		}
	}
}	

void WindowEventHandler::handleDocked(Widget * widget)
{
	previousStatus = widget->getParent()->getFlags();
	widget->getParent()->setFlags( previousStatus & (~WIDGET_DRAGABLE) );
	previousX = widget->getParent()->getX();
	previousY = widget->getParent()->getY();
	widget->getParent()->moveTo(-0.675f*widget->getParent()->getXScale(),-0.825f*widget->getParent()->getYScale()); 

}

void WindowEventHandler::handleUndock(Widget *widget)
{

	widget->getParent()->setFlags(widget->getParent()->getFlags() | (previousStatus & WIDGET_DRAGABLE) );
	widget->getParent()->moveTo(previousX,previousY);
}