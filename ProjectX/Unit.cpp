#include "Unit.h"
#include "RenderManager.h"
#include <stack>
#include <cmath>

Unit::Unit(std::string type, std::vector<std::string> weaknesses, std::vector<std::string> strengths, vec3 pos, vec3 rot, vec3 scale, 
	vec3 rng, int arm, int hp, int dmg, float sp, int unitCount, int unitsInRow, std::string mesh, std::string texture, std::string shader)
	: unitType(type), unitWeaknesses(weaknesses), unitStrengths(strengths), position(pos), rotation(rot), scalar(scale), range(rng), armor(arm), health(hp), damage(dmg), speed(sp), 
	numberOfTroops(unitCount), unitsPerRow(unitsInRow), meshName(mesh), textureName(texture), shaderName(shader), slowEffect(false)
{
	// setting up range box
	rangeBox = new BoundingBox(position, range);
		
	// creating AABB, grabbing vertex count and passing in to create temporary boundingBox for a single unit
	// so we can then calculate the larger BoundingBox
	int count = RenderManager::getInstance()->getRenderableToMesh(meshName.c_str())->getVertexCount();
	float * test = new float[count];
	RenderManager::getInstance()->getRenderableToMesh(meshName.c_str())->getVertData(test);
	BoundingBox temp = BoundingBox(position, test, count, scalar);
	
	soldiersDimensions = temp.getDimensions();
	
	vec3 dimensions;

	for(int i = 0; i < numberOfTroops / 2; i++)
	{
		dimensions.x += soldiersDimensions.x;
		dimensions.y += soldiersDimensions.y;
		dimensions.z = soldiersDimensions.z;
	}
	
	collisionBox = new BoundingBox(position, dimensions);
}

// may seem like a convuluted way to draw things but to position them well enough for a single
// bounding box it's required. However I'm unsure if it'd be more beneficial to draw them basically
// spaced apart and then check for collisions for every single soldier in the unit instead of one overarching box
// would be more accurate for detection. May also be a good idea to pre-calculate the small amount of maths
// required to see if things are even and calculate it every time a unit dies. I know small optimiziation but...
void Unit::draw(mat4 viewProjection)
{
	std::stack<mat4> mStack;
	bool columnEven = false;
	bool rowEven = false; 
	vec3 positions;
	float farLeft;

	// divide by unitsPerRow and round up to get our row count
	int rowCount = ceil((float)numberOfTroops / unitsPerRow);
	
	if(rowCount % 2 == 0)
		rowEven = true;

	if(unitsPerRow % 2 == 0)
		columnEven = true;

	int sideValues = unitsPerRow / 2; 
	int upValues = rowCount / 2;
	
	// applying rotations to the whole unit rather than individuals
	mStack.push(mat4(1.0));
	mStack.top() = translate(mStack.top(), position);
	mStack.top() = rotate(mStack.top(), rotation.x, vec3(1, 0, 0));
	mStack.top() = rotate(mStack.top(), rotation.y, vec3(0, 1, 0));
	mStack.top() = rotate(mStack.top(), rotation.z, vec3(0, 0, 1));
	
	if(columnEven == false)
	{
		// moving position x to the farthest away model on the left
		for(int i = 0; i < sideValues; i++)
			positions.x -= soldiersDimensions.x * 4;

	}
	else
	{
		positions.x = -soldiersDimensions.x * 2;
		
		// moving position x to the farthest away model on the left
		for(int i = 0; i < sideValues-1; i++)
			positions.x -= soldiersDimensions.x * 4;
	}

	farLeft = positions.x; // we move left to right so we save the far left to go back to it
	
	if(rowEven == true)
	{
		positions.y = soldiersDimensions.y * 2; // moving position into the center of a model
		
		// moving positio y to top most row
		for(int i = 0; i < upValues; i++)
			positions.y += soldiersDimensions.y * 4;
	}
	else
	{
		positions.y = soldiersDimensions.y * 4;
		// moving positio y to top most row
		for(int i = 0; i < upValues; i++)
			positions.y += soldiersDimensions.y * 4;
	}
	
	for(int i = 0; i < numberOfTroops; i++)
	{
		// now traverse left then right, before moving up a row till upValues are reached
			
		if(i % unitsPerRow == 0)
		{
			positions.x = farLeft;
			positions.y -= soldiersDimensions.y * 4; 
		}
			
		// draw object
		mStack.push(mStack.top());
		mStack.top() = translate(mStack.top(), positions);
		mStack.top() = scale(mStack.top(), scalar);

		// combining the viewProjection(Projection * view) and the model matrix to make the MVP(projection * view * model) 
		mat4 temp = viewProjection * mStack.top();
		ShaderManager::getInstance()->getShader(shaderName.c_str())->setUniformMatrix4fv("MVP", 1, false, value_ptr(temp));  //set the MVP
		TextureManager::getInstance()->bind(textureName.c_str()); // Find and bind that texture
		RenderManager::getInstance()->renderRenderable(meshName.c_str()); // render that renderable on the screen
		mStack.pop();

		positions.x += soldiersDimensions.x * 4;
	}



		
	// below for test and display purposes only, shows the position of the group as a whole
	// and thus where the bbox originates from and the group rotates around.

	mStack.push(mat4(1.0));
	mStack.top() = translate(mStack.top(), position);
	mStack.top() = scale(mStack.top(), scalar);
	mStack.top() = rotate(mStack.top(), rotation.x, vec3(1, 0, 0));
	mStack.top() = rotate(mStack.top(), rotation.y, vec3(0, 1, 0));
	mStack.top() = rotate(mStack.top(), rotation.z, vec3(0, 0, 1));

	mat4 temp = viewProjection * mStack.top();
	ShaderManager::getInstance()->getShader(shaderName.c_str())->setUniformMatrix4fv("MVP", 1, false, value_ptr(temp));  //set the MVP
	TextureManager::getInstance()->bind(textureName.c_str()); // Find and bind that texture
	RenderManager::getInstance()->renderRenderable(meshName.c_str()); // render that renderable on the screen
	mStack.pop();
	
}


// very rudimentary version of this function for demo purposes, basically increments using speed
// to a point in the current path list(directly) it currently doesn't loop through a list it only checks the first
// since we're only passing in a single value for an intersection on an object for test purposes. The
// + 0.1 and - 0.1 on the checks are to stop float precision errors causing the object to jitter as it 
// never exactly reaches the point we're aiming for!
void Unit::traversePath()
{
	if(!currentPath.empty())
	{
			
		if(position.x < currentPath[0].x - 0.1)
		{
			position.x += speed;
		}
		else if(position.x > currentPath[0].x + 0.1)
		{	
			position.x -= speed;
		}

		if(position.y < currentPath[0].y - 0.1)
		{
			position.y += speed;
		}
		else if(position.y > currentPath[0].y + 0.1)
		{
			position.y -= speed;	
		}

		// calculating the angle of the unit in orientation to the current path position 
		float check = (atan2(currentPath[0].x - position.x, currentPath[0].y - position.y))*57.29577951f;
	
		// setting it so that the enemy is facing the player(adding 180 on so that the character doesn't face away from the player)
		rotation.z = 180 - check;

		if(position.x > currentPath[0].x - 0.1 && position.x < currentPath[0].x + 0.1 
			&& position.y > currentPath[0].y - 0.1 && position.y < currentPath[0].y + 0.1 )
		{
			currentPath.erase(currentPath.begin());
		}
	
	}
}



// will be filled out next semester once we start to work on combat
void Unit::calculateCombat(Unit* attacker)
{

}

// basic operator overloads so that we can tell if a unit is >(stronger), <(weaker) or ==(equal) to another unit 
bool Unit::operator > (const Unit* param)
{
	for(int i = 0; i < this->unitStrengths.size(); i++)
		for(int i2 = 0; i2 < param->unitWeaknesses.size(); i2++)
			if(this->unitStrengths[i] == param->unitWeaknesses[i2])
				return true;

	return false;
}

bool Unit::operator < (const Unit* param)
{
	for(int i = 0; i < this->unitWeaknesses.size(); i++)
		for(int i2 = 0; i2 < param->unitStrengths.size(); i2++)
			if(this->unitWeaknesses[i] == param->unitStrengths[i2])
				return true;

	return false;
}

bool Unit::operator == (const Unit* param)
{
	if(this->unitType == param->unitType)
		return true;
	else
		return false;
}
	
Unit::~Unit()
{
	delete collisionBox;
	delete rangeBox;
}