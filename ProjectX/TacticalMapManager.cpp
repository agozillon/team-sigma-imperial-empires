#include "TacticalMapManager.h"
#include <iostream>

#include <glm\glm.hpp>
#include <glm\gtc\matrix_transform.hpp>
#include <glm\gtc\type_ptr.hpp>

#include "ShaderManager.h"
#include "CameraManager.h"
#include "RenderManager.h"
#include "TextureManager.h"
#include "GameManager.h"
#include "Mouse.h"

#include <SDL.h>
#include <stack>
#include <stdio.h>

TacticalMapManager* TacticalMapManager::instance = NULL;

void TacticalMapManager::createInstance()
{
	if( instance ) return;

	instance = new TacticalMapManager();
}


void TacticalMapManager::deleteInstance()
{

	if( !instance) return;

	delete instance;
	instance = NULL;
}

TacticalMapManager* TacticalMapManager::getInstance()
{
	if( !instance ) createInstance();

	return instance;
}



TacticalMapManager::TacticalMapManager()
{
	
	CameraManager::getInstance()->addCamera("TacticalMap", vec3(0.0f, 0.0f, 0.0),vec3(0.0f, 0.0f, -1.0f),vec3(0.0f, 1.0f, 0.0f));
	CameraManager::getInstance()->setActiveCamera("TacticalMap");
	CameraManager::getInstance()->rotateCamera(0.0f, 30.0f);

	Mouse::getInstance()->updateViewMatrix(CameraManager::getInstance()->lookAt());

	//This is a quick "short cut function" that I put in to easily create a shader program form 2 shaders 
	ShaderManager::getInstance()->initShaderProgram("simpleTac","simpleTac.vert","simpleTac.frag","simpleTac.vert","simpleTac.frag");
	ShaderManager::getInstance()->use("simpleTac");//just the same as usual glUse(program)
	
	//this is actually already done in gameManager, but basicly it loads in the bmp and adds it to the big list of textures
	TextureManager::getInstance()->load("RealGrass","grass3.bmp",BMP);
	TextureManager::getInstance()->load("Tree1","arboltexture.bmp",BMP);
	TextureManager::getInstance()->load("Log","Wood.bmp",BMP);
	TextureManager::getInstance()->load("Tree2","pino2.bmp",BMP);
	TextureManager::getInstance()->load("Tree3","diffuse_pine.bmp",BMP);
	TextureManager::getInstance()->load("Tree4","huge.bmp",BMP);
	

	RenderManager::getInstance()->loadMesh("Cube", "cube.obj");
	RenderManager::getInstance()->loadMesh("Tree1", "arboltree.obj");
	RenderManager::getInstance()->loadMesh("Log", "log.obj");
	RenderManager::getInstance()->loadMesh("Tree2", "pino2.obj");
	RenderManager::getInstance()->loadMesh("Tree3", "pine_tree.obj");
	RenderManager::getInstance()->loadMesh("Tree4", "huge_tree.obj");
	
	std::vector<std::string> tests;
	test = new Unit("Test", tests, tests, vec3(-10, 0, -4.8), vec3(0, 0, -90), vec3(0.1, 0.1, 0.1), vec3(20, 20, 20), 4, 4, 4, 0.1, 28, 7, "Cube", "Fabric",  "simpleTac");
	test2 = new Unit("Test", tests, tests, vec3(0, 0, -4.8), vec3(0, 0,  90), vec3(0.5, 0.5, 0.5), vec3(20, 20, 20), 4, 4, 4, 10, 34, 10, "Log", "Fabric",  "simpleTac");
	
	levelManager = new LevelManager();

	glEnable(GL_DEPTH_TEST);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
	glDepthMask(GL_TRUE);
}

TacticalMapManager::~TacticalMapManager()
{
	
}


void TacticalMapManager::draw()
{
	glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
	glDepthMask(GL_TRUE);


	glm::mat4 projection = RenderManager::getInstance()->getProjection(); // usual GLM stuff
	glm::mat4 view = CameraManager::getInstance()->lookAt();
	glm::mat4 viewProjection = projection * view;

	levelManager->draw();

	test->draw(viewProjection);
	test2->draw(viewProjection);
}

void TacticalMapManager::update()
{
	levelManager->update(); // update the level, moving objects etc.
	
	test->traversePath(); // unit traverse the path!
}


void TacticalMapManager::checkKeyPress()
{
	Uint8 * keys = SDL_GetKeyboardState(NULL);

	// move the camera to the left/right/up/down if a WASD key is pressed or the mouse is
	// at the edge of the screen. Each of the mouse at edge of screen checks have a different 
	// offset because using the WASD keys to move the screen actually jumps the mouse co-ordinates
	// to invalid values which coincidently go below the threshold in some cases making the screen
	// continue to scroll until the mouse is moved again. The various offsets stop this whilst keeping
	// the scrolling sensitive.
	if( keys[SDL_SCANCODE_W] || Mouse::getInstance()->getY() > GameManager::getInstance()->getHeight() - 10)  
	{
		CameraManager::getInstance()->moveCameraUp(0.1f);
		Mouse::getInstance()->updateViewMatrix(CameraManager::getInstance()->lookAt());
	}

	if( keys[SDL_SCANCODE_S] || Mouse::getInstance()->getY() <= 10)
	{
		CameraManager::getInstance()->moveCameraUp(-0.1f);
		Mouse::getInstance()->updateViewMatrix(CameraManager::getInstance()->lookAt());
	}

	if( keys[SDL_SCANCODE_A] || Mouse::getInstance()->getX() <  4 ) 
	{
		CameraManager::getInstance()->moveCameraRight(-0.1f);
		Mouse::getInstance()->updateViewMatrix(CameraManager::getInstance()->lookAt());
	}

	if( keys[SDL_SCANCODE_D] || Mouse::getInstance()->getX() > GameManager::getInstance()->getWidth() - 5)
	{
		CameraManager::getInstance()->moveCameraRight(0.1f); 
		Mouse::getInstance()->updateViewMatrix(CameraManager::getInstance()->lookAt());
	}

	if( keys[SDL_SCANCODE_Z] )
	{
		CameraManager::getInstance()->moveCameraForward(0.1f);
		Mouse::getInstance()->updateViewMatrix(CameraManager::getInstance()->lookAt());
	}

	if( keys[SDL_SCANCODE_X])
	{
		CameraManager::getInstance()->moveCameraForward(-0.1f);
		Mouse::getInstance()->updateViewMatrix(CameraManager::getInstance()->lookAt());
	}

	if( keys[SDL_SCANCODE_COMMA] )
	{
		CameraManager::getInstance()->rotateCamera(0.0f, -1.0);
	}

	if( keys[SDL_SCANCODE_PERIOD] )
	{
		CameraManager::getInstance()->rotateCamera(0.0f, 1.0f);
	}

		// check collisions with the mouse to any object then pass in the position of the 
	// intersection to the unit to traverse to it
	if(Mouse::getInstance()->getLeftState() == BUTTON_STATE_DOWN)
	{
		Mouse::getInstance()->calculateRay();

		std::vector<Scenery*> scene;
		levelManager->getScenery(scene);
		glm::vec3 temp;
		std::vector<glm::vec3> tempV;

		for(int i = 0; i < scene.size(); i++)
		{
			if(scene[i]->getCollisionBox()->detectRayCollision(Mouse::getInstance()->getRayOrigin(), Mouse::getInstance()->getRayDirection(), temp))
			{
				tempV.push_back(temp);
				test->updatePath(tempV);
			}
		}
	}

	if( keys[SDL_SCANCODE_ESCAPE]) GameManager::getInstance()->changeState(MAIN_MENU);
}