#ifndef WIDGET_CHECKBOX_H
#define WIDGET_CHECKBOX_H
#include "Button.h"



class Checkbox: public Button
{
public:
	Checkbox(const Button & button): Button(button) { down = false; illuminationMode |= BUTTON_ONCLICK;}


	virtual bool checkMouse()
	{
		bool returnee = Button::checkMouse();

		if( status & STATUS_MOUSE_LEFT_RELEASE )
		{
			illuminationMode  ^= BUTTON_ALWAYS;
			down = !down;
		}

		return returnee;
	}

	//Returns true if the button has been clicked or false if it hasn't
	inline bool isChecked() const { return !down; }
protected:
	bool down;

};


#endif