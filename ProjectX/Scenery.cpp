#include "Scenery.h"
#include "RenderManager.h"
#include <stack>

Scenery::Scenery(vec3 pos, vec3 rot, vec3 scale, std::string mesh, std::string texture, std::string shader)
	: position(pos), rotation(rot), scalar(scale), meshName(mesh), textureName(texture), shaderName(shader)
{
	
	// creating AABB, grabbing vertex count and passing in
	int count = RenderManager::getInstance()->getRenderableToMesh(meshName.c_str())->getVertexCount();
	float * test = new float[count];
	RenderManager::getInstance()->getRenderableToMesh(meshName.c_str())->getVertData(test);
	collisionBox = new BoundingBox(position, test, count, scalar);

}

void Scenery::draw(mat4 viewProjection)
{
	std::stack<mat4> mStack;


	// calculating the model matrix, from this scenerys position, scale and rotation
	mStack.push(mat4(1.0));
	mStack.top() = translate(mStack.top(), position);
	mStack.top() = scale(mStack.top(), scalar);
	mStack.top() = rotate(mStack.top(), rotation.x, vec3(1, 0, 0));
	mStack.top() = rotate(mStack.top(), rotation.y, vec3(0, 1, 0));
	mStack.top() = rotate(mStack.top(), rotation.z, vec3(0, 0, 1));

	// combining the viewProjection(Projection * view) and the model matrix to make the MVP(projection * view * model) 
	mat4 temp = viewProjection * mStack.top();
	ShaderManager::getInstance()->getShader(shaderName.c_str())->setUniformMatrix4fv("MVP", 1, false, value_ptr(temp));  //set the MVP
	TextureManager::getInstance()->bind(textureName.c_str()); // Find and bind that texture
	RenderManager::getInstance()->renderRenderable(meshName.c_str()); // render that renderable on the screen
	mStack.pop();
}

Scenery::~Scenery()
{
	delete collisionBox;
}