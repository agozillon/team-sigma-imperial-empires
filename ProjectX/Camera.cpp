#include "Camera.h"
#define DEG_TO_RADIAN 0.017453293


Camera::Camera(const glm::vec3 &e,const glm::vec3 &t, const glm::vec3& u)
{
	eye =e;
	target = t;
	up = u;
	yaw = 0;
	pitch = 0;
}


Camera::~Camera()
{
}


glm::mat4 Camera::lookAt()
{
	return glm::lookAt(eye,target,up);
}

void Camera::setEye(const glm::vec3 &e)
{
	eye = e;
}

void Camera::setTarget(const glm::vec3 &t)
{
	target = t;
}

void Camera::setUp(const glm::vec3 &u)
{
	up = u;
}

// simple camera move right(or left) function from RT3D
void Camera::moveCameraRight(const float dist)
{
	eye = glm::vec3(eye.x + dist*std::cos(yaw*DEG_TO_RADIAN), eye.y, eye.z + dist*std::sin(yaw*DEG_TO_RADIAN)); // left
	target = glm::vec3(eye.x + 1.0f*std::sin(yaw*DEG_TO_RADIAN), eye.y + 1.0 * std::tan(pitch*DEG_TO_RADIAN), eye.z - 1.0f*std::cos(yaw*DEG_TO_RADIAN));
}


// Camera move up function it works very basically compared to the rest(doesn't change with angle)
// which we don't want since pitch allows us to position the camera easily.
void Camera::moveCameraUp(const float dist)
{
	eye = glm::vec3(eye.x, eye.y+ dist, eye.z);
	target = glm::vec3(eye.x + 1.0f*std::sin(yaw*DEG_TO_RADIAN), eye.y + 1.0 * std::tan(pitch*DEG_TO_RADIAN), eye.z - 1.0f*std::cos(yaw*DEG_TO_RADIAN));
}

// not really needed, just added incase we require a zoom feature at some point basic 
// camera move forward from RT3D
void Camera::moveCameraForward(const float dist)
{
	eye = glm::vec3(eye.x + dist*std::sin(yaw*DEG_TO_RADIAN),  eye.y + dist * std::tan(pitch*DEG_TO_RADIAN), eye.z - dist*std::cos(yaw*DEG_TO_RADIAN));
	target = glm::vec3(eye.x + 1.0f*std::sin(yaw*DEG_TO_RADIAN), eye.y + 1.0 * std::tan(pitch*DEG_TO_RADIAN), eye.z - 1.0f*std::cos(yaw*DEG_TO_RADIAN));
}

void Camera::rotateCamera(const float y, const float p)
{
	yaw += y;
	pitch += p;

	target = glm::vec3(eye.x + 1.0*std::sin(yaw*DEG_TO_RADIAN), eye.y + 1.0 * std::tan(pitch*DEG_TO_RADIAN), eye.z - 1.0*std::cos(yaw*DEG_TO_RADIAN));
}