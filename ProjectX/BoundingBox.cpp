#include "BoundingBox.h"
#include <glm/gtc/matrix_transform.hpp> // including glm matrix_transforms for access to matrix transforms
#include <iostream>

// constructor that accepts a position and a meshes vertices and a count of them and a scalar, from these
// it calculates the AABB size, positions it and scales it relevantly(scalars should be same size as the amount
// you scale the mesh by!)
BoundingBox::BoundingBox(const vec3 pos, const float * meshVertices, const int numberOfVertices, const vec3 scalar): scale(scalar),
	position(pos)
{

	// looping through making sure the values we're checking are positive
	// and finding the largest vertice on the x, y and z to create the bounding box
	for(int i = 0; i < numberOfVertices; i+=3)
	{
		float tempVal;

		if(meshVertices[i] < 0)
			tempVal = -meshVertices[i];
		else
			tempVal = meshVertices[i];

		if(tempVal > dimensions.x)
		   dimensions.x = tempVal;

		if(meshVertices[i+1] < 0)
			tempVal = -meshVertices[i+1];
		else
			tempVal = meshVertices[i+1];

		if(tempVal > dimensions.y)
		   dimensions.y = tempVal;

		if(meshVertices[i+2] < 0)
			tempVal = -meshVertices[i+2];
		else
			tempVal = meshVertices[i+2];

		if(tempVal > dimensions.z)
		   dimensions.z = tempVal;
	}
	
	dimensions = dimensions * scale;
}

// basic constructor that accepts the BoundingBox's position and dimensions and sets them up, USED IN RT3D of Andrew Gozillon
BoundingBox::BoundingBox(const glm::vec3 bboxPositions, const glm::vec3 bboxDimensions)
{
	dimensions.x = bboxDimensions.x;
	dimensions.y = bboxDimensions.y;
	dimensions.z = bboxDimensions.z;

	position.x = bboxPositions.x;
	position.y = bboxPositions.y;
	position.z = bboxPositions.z;
}


// function that accepts a pointer to a BoundingBox and checks for a collision between the current bounding box
// and the passed in BoundingBox and then returns either true or false depending on if they've colided
// USED IN RT3D of Andrew Gozillon
const bool BoundingBox::detectBoxCollision(BoundingBox * colideeBoundingBox)
{
	// sets the initial collision return bool to false
	bool collision = false;

	// really simple Axis alligned bounding box detection checks if one side of a bounding box is in another
	// and then returns a true value if it is indented this way for ease of reading
	if ((position.x + dimensions.x > colideeBoundingBox->getPosition().x - colideeBoundingBox->getDimensions().x) 
	&& (position.x - dimensions.x < colideeBoundingBox->getPosition().x + colideeBoundingBox->getDimensions().x)
	&& (position.y + dimensions.y > colideeBoundingBox->getPosition().y - colideeBoundingBox->getDimensions().y)
	&& (position.y - dimensions.y < colideeBoundingBox->getPosition().y + colideeBoundingBox->getDimensions().y)					
	&& (position.z + dimensions.z > colideeBoundingBox->getPosition().z - colideeBoundingBox->getDimensions().z)						
	&& (position.z - dimensions.z < colideeBoundingBox->getPosition().z + colideeBoundingBox->getDimensions().z))
	{
		collision = true;	
	}

	// returns the bool 
	return collision;
}

// 3d ray collision check, had a lot of help from online sources(since my maths isn't great) however it
// calculates where a ray line intersects the 6 planes of the bounding box (represented as a line)
const bool BoundingBox::detectRayCollision(const glm::vec3 rayPoint, const glm::vec3 rayDirection, glm::vec3 &collisionPoint)
{
	bool collision = true;
	
	glm::vec3 max, min;
	glm::vec3 bMax, bMin;


	// calculating min/max of bboxs x, y and z dimensions each of these max and min values
	// creates our bounding box planes for that specific dimension
	bMax.x = position.x + dimensions.x; bMin.x = position.x - dimensions.x;
	bMax.y = position.y + dimensions.y;	bMin.y = position.y - dimensions.y;
	bMax.z = position.z + dimensions.z; bMin.z = position.z - dimensions.z;
	
	// calculating where on the Bounding Boxs X plane the ray interects
	// the min 
	if(rayDirection.x >= 0)
	{
		// solving for 2 lines bMax is one rayPoint is one, we get the direction
		// from the rays origin to the bounding boxs max x and then divide it by
		// the rays direction to see where it crosses that plane at a -ve value
		// means it's behind the rays origin so the ray intersected it some point "in the past"
		// another way of looking at it (my prefferred way for visualizing it at least so may not be
		// accurate) is that we're getting the number of increments of the rays direction
		// to get to the bounding boxes minimum and maximum ranges I.E ray direction x at 0.5
		// box x min range = 2, 2 / 0.5 = 4 thus it takes 4 increments of the ray to get here
		max.x = (bMax.x - rayPoint.x) / rayDirection.x;
		min.x = (bMin.x - rayPoint.x) / rayDirection.x;
	}
	else
	{
		min.x = (bMax.x - rayPoint.x) / rayDirection.x;
		max.x = (bMin.x - rayPoint.x) / rayDirection.x;
	}

	if(rayDirection.y >= 0)
	{
		max.y = (bMax.y - rayPoint.y) / rayDirection.y;
		min.y = (bMin.y - rayPoint.y) / rayDirection.y;
	}
	else
	{
		min.y = (bMax.y - rayPoint.y) / rayDirection.y;
		max.y = (bMin.y - rayPoint.y) / rayDirection.y;
	}
		
	// basically if the rays min.x intersect point is greater than
	// the rays max y intersect point or min.y is greater than max.x 
	// then it doesn't intersect as the ray is travelling between two planes
	// that never allow for a connection within the object I.E overshoots
	// min x should be lower than max y and min y should be lower than max x 
	// for a ray to travel between them!
	// My visualized (and perhaps inaccurate) way of looking at it is that 
	// if the minimum x's increments are greater than the maximum y's increments
	// then the ray overshoots / goes above our box e.g min x increments = 10 to get
	// to the desired plane/location and max y increments = 8 to get to its desired plane
	// box min x = 1 box max y = 2, ray x = 0.10 ray y = 0.25, 10 * 0.10 = 1 y with 10 increments
	// = 2.50! so when we just reach the minimum x we've overshot the maximum y by 2 increments!
	if(min.x > max.y || min.y > max.x)
		return false;

	// these have no bearing what so ever on the check
	// it's essentially our min and max scale value to scale the ray direction with
	// to help get the intersection point. we want the smallest number and largest of the increments
	//to increment our ray to get the very first touch of the cube instead of too far in etc
	if(min.y > min.x)
		min.x = min.y;

	if(max.y < max.x)
		max.x = max.y;

	// calculating a z plane using the min and max, checks if the ray
	// is greater or smaller as this will change which is the min and which is the max
	// of the bounding box in relation to the rays direction
	if(rayDirection.z >= 0)
	{
		max.z = (bMax.z - rayPoint.z) / rayDirection.z;
		min.z = (bMin.z - rayPoint.z) / rayDirection.z;
	}
	else
	{
		min.z = (bMax.z - rayPoint.z) / rayDirection.z;
		max.z = (bMin.z - rayPoint.z) / rayDirection.z;
	}

	if(min.x > max.z || min.z > max.x)
		return false;

	if(min.z > min.x)
		min.x = min.z;

	if(max.z < max.x)
		max.x = max.z;

	// rays origin point + the number of ray increments to the intersect point
	// multiplied by our rays direction in all cases
	collisionPoint =  rayPoint + (min.x * rayDirection);
	
	return collision;
}


