#include "ShaderManager.h"
#include <iostream>

ShaderManager * ShaderManager::instance = NULL;

ShaderManager::ShaderManager()
{
	nullProgram = new ShaderProgram("NULL");
}

void ShaderManager::use(const std::string &name)
{	
	std::map<std::string,ShaderProgram*>::const_iterator itr = shaderPrograms.find(name);
	if( itr == shaderPrograms.end() ) return;

	shaderPrograms[name]->use();
	
	for(itr = shaderPrograms.begin(); itr != shaderPrograms.end(); itr++)
	{
		if( itr->second->isActive() && itr != shaderPrograms.find(name) ) // if the shader was active before, mark it as deactivate it 
		{
			itr->second->deactivate();
		}
	}


}


ShaderManager::~ShaderManager()
{
	delete nullProgram;

	std::map<std::string,Shader*>::const_iterator shaderItr = shaders.begin();

	while( shaderItr != shaders.end() )
	{
		delete shaderItr->second;
		shaderItr++;
	}
	shaders.clear();

	std::map<std::string,ShaderProgram*>::const_iterator programItr= shaderPrograms.begin();
	while( programItr != shaderPrograms.end() )
	{
		delete programItr->second;
		programItr++;
	}
}


void ShaderManager::createProgram(const std::string name)
{
	shaderPrograms[name] = new ShaderProgram(name);
}

void ShaderManager::createShader(const std::string name,ShaderType t)
{
	shaders[name] = new Shader(name,t);
}

void ShaderManager::compileShader(const std::string name)
{
	std::map<std::string,Shader *>::const_iterator i = shaders.find(name);

	if( i != shaders.end() )
	{
		i->second->compile();
	}
}

void ShaderManager::loadShader(const std::string name,const std::string filePath)
{
	std::map<std::string,Shader*>::const_iterator i = shaders.find(name);

	if( i != shaders.end() )
	{
		i->second->load(filePath);
	}
}

void ShaderManager::attachShader(const std::string pName,const std::string sName)
{
	std::map<std::string,Shader*>::const_iterator iShader = shaders.find(sName);
	std::map<std::string,ShaderProgram*>::const_iterator iProgram = shaderPrograms.find(pName);

	if( iShader != shaders.end() && iProgram != shaderPrograms.end() )
	{
		iProgram->second->attachShader(iShader->second);
	} else std::cout << "Cannot attach shader: " << sName << " to program: " << pName << std::endl;
}


ShaderProgram * ShaderManager::operator[](std::string name)
{
	std::map<std::string,ShaderProgram*>::const_iterator i = shaderPrograms.find(name);

	if( i != shaderPrograms.end() ) 
	{
		return i->second;
	} else 
	{
		return nullProgram;
	}
}

void ShaderManager::link(const std::string name)
{
	std::map<std::string,ShaderProgram*>::const_iterator itr = shaderPrograms.find(name);
	
	if( itr != shaderPrograms.end() )
		itr->second->link();

}


void ShaderManager::createInstance()
{
	instance = new ShaderManager();
}

void ShaderManager::deleteInstance()
{
	delete instance;
}

ShaderManager * ShaderManager::getInstance()
{
	return instance;
}


void ShaderManager::initShaderProgram(const std::string name,const std::string vName,const std::string fName,const std::string vPath,const std::string fPath)
{

	createProgram(name);
	createShader(vName,VERTEX);
	createShader(fName,FRAGMENT);

	loadShader(vName,vPath);
	loadShader(fName,fPath);

	compileShader(vName);
	compileShader(fName);

	attachShader(name,vName);
	attachShader(name,fName);

	link(name);	

}


ShaderProgram * ShaderManager::getShader(const std::string name)
{
	return (*this)[name];
}
