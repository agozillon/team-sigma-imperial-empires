#include "CameraManager.h"

CameraManager * CameraManager::instance =NULL; 

CameraManager::CameraManager()
{
	camera = "NULL";
	addCamera("NULL",glm::vec3(0.0f,0.0f,0.000000001f),glm::vec3(0.0f,0.0f,1.0f),glm::vec3(0.0f,1.0f,0.0f));

}

CameraManager::~CameraManager()
{
}


void CameraManager::addCamera(const std::string &name,const glm::vec3&eye,const glm::vec3&target,const glm::vec3&up)
{
	if( cameras.find(name) != cameras.end() ) return;
	cameras[name] = new Camera(eye,target,up);
}


void CameraManager::setActiveCamera(const std::string & name)
{
	if( cameras.find(name) == cameras.end() ) return;
	camera = name;
}


void CameraManager::createInstance()
{
	if( instance ) return;
	instance = new CameraManager();
	
}

void CameraManager::deleteInstance()
{
	if( !instance ) return;
	delete instance;
	instance = NULL;
	
}

CameraManager * CameraManager::getInstance()
{
	if( !instance ) createInstance();
	return instance;
}

glm::mat4 CameraManager::lookAt()
{
	return cameras[camera]->lookAt();
}

void CameraManager::setTarget(const glm::vec3 & target)
{
	cameras[camera]->setTarget(target);
}


void CameraManager::setEye(const glm::vec3 & eye)
{
	cameras[camera]->setEye(eye);
}

void CameraManager::setUp(const glm::vec3 & up)
{
	cameras[camera]->setUp(up);
}

void CameraManager::moveCameraRight( float dist)
{
	cameras[camera]->moveCameraRight(dist);
}

void CameraManager::moveCameraUp( float dist)
{
	cameras[camera]->moveCameraUp(dist);
}

void CameraManager::moveCameraForward( float dist)
{
	cameras[camera]->moveCameraForward(dist);
}

void CameraManager::rotateCamera(const float y, const float p)
{
	cameras[camera]->rotateCamera(y, p);
}