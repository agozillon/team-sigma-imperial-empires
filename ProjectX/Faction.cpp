#include "Faction.h"


Faction::Faction()
{
}


Faction::Faction(const std::string&n,unsigned int i,const glm::vec4 & v)
{
	name   = n;
	index  = i;
	colour = v;
}


const glm::vec4& Faction::getColour() const
{
	return colour;
}

const unsigned int Faction::getIndex() const
{
	return index;
}

const std::string & Faction::getName() const
{
	return name;
}