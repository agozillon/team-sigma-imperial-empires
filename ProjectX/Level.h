#ifndef LEVEL_H
#define LEVEL_H
#include "Scenery.h"
#include <vector>
#include <string>

class Level{
public:
	virtual ~Level(){}
	virtual void init() = 0;
	virtual void draw() = 0;
	virtual void update() = 0;
	virtual void getScenery(std::vector<Scenery*> &scenery) = 0;
};
#endif