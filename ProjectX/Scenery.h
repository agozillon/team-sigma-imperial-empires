#ifndef SCENERY_H
#define SCENERY_H
#include "WorldObjects.h"
#include <string>
using namespace glm;	

class Scenery : public WorldObjects
{
public:
	const inline vec3 getPosition(){return position;}
	const inline vec3 getRotation(){return rotation;}
	const inline vec3 getScalar(){return scalar;}
	inline void updatePosition(vec3 pos){position = pos;}
	inline void updateRotation(vec3 rot){rotation = rot;}
	inline void updateScalar(vec3 scale){scalar = scale;}
	void draw(mat4 viewProjection);
	inline BoundingBox* getCollisionBox(){return collisionBox;}
	~Scenery();
	Scenery(vec3 pos, vec3 rot, vec3 scale, std::string mesh, std::string texture, std::string shader);

private:
	vec3 scalar;
	vec3 rotation;
	vec3 position;
	BoundingBox * collisionBox;
	std::string meshName;
	std::string textureName;
	std::string shaderName;
};

#endif