#include "Mouse.h"
#include "GameManager.h"
#include "CameraManager.h"
#include <iostream>
#include <math.h>

Mouse * Mouse::instance = nullptr;
Mouse::Mouse(){ }

void Mouse::createInstance()
{
	if( instance ) return;
	instance = new Mouse();
}

Mouse * Mouse::getInstance()
{
	if( !instance ) createInstance();
	return instance;
}

void Mouse::deleteInstance()
{
	if( !instance ) return;
	delete instance;
	instance = NULL;
}

void Mouse::updateEvent(const SDL_Event e)
{
	currentEventState = e;

	timeStamp = e.button.timestamp;
	
	x = e.motion.x;
	y = GameManager::getInstance()->getHeight() - e.motion.y;

	if( e.button.button == SDL_BUTTON_LEFT && e.button.type == SDL_MOUSEBUTTONDOWN )
		left = BUTTON_STATE_DOWN;
	 else if(e.button.button == SDL_BUTTON_LEFT && e.button.type == SDL_MOUSEBUTTONUP )
		left = BUTTON_STATE_UP;
	

	if( e.button.button == SDL_BUTTON_RIGHT && e.button.type == SDL_MOUSEBUTTONDOWN )
		right = BUTTON_STATE_DOWN;
	 else if(e.button.button == SDL_BUTTON_RIGHT && e.button.type == SDL_MOUSEBUTTONUP )
		right = BUTTON_STATE_UP;
	

	if( e.button.button == SDL_BUTTON_MIDDLE && e.button.type == SDL_MOUSEBUTTONDOWN )
		middle = BUTTON_STATE_DOWN;
	else if(e.button.button == SDL_BUTTON_MIDDLE && e.button.type == SDL_MOUSEBUTTONUP )
		middle = BUTTON_STATE_UP;
}



// kinda needs to be edited to be more maleable, as changing the perspective projection will break it right now
void Mouse::calculateRay()
{
	// getting the screens aspect ratio
	float aspectRatio = (float)GameManager::getInstance()->getWidth() / GameManager::getInstance()->getHeight();
	
	// re-allgining the mouses x 0 - width and y height - 0 co-ordinates to screen space co-ordinates of 
	// -1 to 1, so window space to NDC space
	double screenSpaceX = (currentEventState.motion.x / (GameManager::getInstance()->getWidth() / 2.0) - 1.0f) * aspectRatio;
	double screenSpaceY = (1.0f - currentEventState.motion.y / (GameManager::getInstance()->getHeight() / 2.0));

	// calculating the view ratio of the cameras projection (the 60.0f should really be the
	// cameras FOV) this will change the position of the NDC space point on sreen/ un-clip it!
	// into camera space when we multiply it later on!
	double viewRatio = std::tan(((float) 3.14159265359 / (180.0f/60.0f) / 2.00f)); 
	
	screenSpaceX = screenSpaceX * viewRatio;
	screenSpaceY = screenSpaceY * viewRatio;

	// should also be the near and far distances of the camera in place of the 1 and 400, we're getting the 
	// camera near and far from the current screen space positions ratio, as the distance increases
	// from the camera the position will change linearly into the world based off of the cameras FOV I.E  \_/
	// lines/rays would go out diagonally into the world but be percieved as straight lines to the viewer
	// so finally getting the camera space positions 
	glm::vec4 cameraNear = glm::vec4((float)(screenSpaceX * 1), (float)(screenSpaceY * 1), -1, 1);
	glm::vec4 cameraFar = glm::vec4((float)(screenSpaceX * 400), (float)(screenSpaceY * 400), -400, 1);

	// now from camera space back to world space using the inverse camera matrix to un-translate, rotate
	// and scale the positions back 
	glm::vec4 worldSpaceNear = invViewMatrix * cameraNear;
	glm::vec4 worldSpaceFar =  invViewMatrix * cameraFar;

	// the origin is now the world space near as we wish to start at the cameras initial point
	rayOrigin = glm::vec3(worldSpaceNear.x, worldSpaceNear.y, worldSpaceNear.z);
	// and the direction is from this origin point to the far plane world space point!
	rayDirection = glm::vec3(worldSpaceFar.x - worldSpaceNear.x, worldSpaceFar.y - worldSpaceNear.y, worldSpaceFar.z - worldSpaceNear.z);
	// and we now normalize it as we do not need the full length distance just the direction
	rayDirection = glm::normalize(rayDirection);
}

Mouse::~Mouse()
{

}
