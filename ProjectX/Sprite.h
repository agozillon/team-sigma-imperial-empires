#ifndef SPRITE_H
#define SPRITE_H
#include "Renderable.h"
#include <string>


class Sprite: public Renderable
{
public:
	Sprite(); // blank sprite will use the null texture
	Sprite(const std::string &textureName);


	~Sprite();
	void render() const;

	inline const std::string & getTexture() const { return texture; }

private:
	std::string texture;
	GLuint VBO; //for UV coords

	void initGL();
};

#endif