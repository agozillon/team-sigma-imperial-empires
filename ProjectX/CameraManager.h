#ifndef CAMERA_MANAGER_H
#define CAMERA_MANAGER_H
#include <map>
#include <string>
#include "Camera.h"


class CameraManager
{
public:

	static void createInstance();
	static void deleteInstance();
	static CameraManager *getInstance();


	void setActiveCamera(const std::string&name);
	void addCamera(const std::string &name,const glm::vec3&eye,const glm::vec3&target,const glm::vec3&up);
	void moveCameraRight( float dist);
	void moveCameraUp( float dist);
	void moveCameraForward( float dist);
	void rotateCamera(const float y, const float p);

	glm::mat4 lookAt();

	void setEye(const glm::vec3& eye);
	void setUp(const glm::vec3& up);
	void setTarget(const glm::vec3& target);


	const glm::vec3& getEye() { return cameras[camera]->getEye();}
private:
	
	std::string camera;
	static CameraManager * instance;

	std::map<std::string,Camera*> cameras;

	CameraManager();
	~CameraManager();
};

#endif