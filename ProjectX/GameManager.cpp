#include "GameManager.h"
#include <iostream>
#include "RenderManager.h"
#include "Sprite.h"
#include "MainMenuState.h"
#include "StrategicMapState.h"
#include "TacticalMapState.h"
#include "CameraManager.h"
#include "UnitData.h"
#include <time.h>
GameManager * GameManager::instance = NULL;

GameManager::GameManager(const unsigned int w,const unsigned int h):
	SCREEN_HEIGHT(h),
	SCREEN_WIDTH(w)
{	
}

void GameManager::checkState()
{

	if( stateId == previousState ) return; //we are already in correct state
	
	if( stateId >= MAIN_MENU && stateId <= TACTICAL_MAP ) // is a valid state
	{
		state->onExit();
		delete state;
		previousState = stateId;
		
		if( stateId == MAIN_MENU )
		{
			state = new MainMenuState();
		} else if( stateId == STRATEGIC_MAP )
		{
			state = new StrategicMapState();
		} else if ( stateId = TACTICAL_MAP ) 
		{
			state = new TacticalMapState();
		}

		state->onEntry();
	}
}
void GameManager::changeState(GameStateIdentifier i)
{
	if( i == stateId ) return; // already on this state
	stateId = i;
}
GameManager::~GameManager()
{
	RenderManager::deleteInstance();
	ShaderManager::deleteInstance();
	TextureManager::deleteInstance();
	CameraManager::deleteInstance();
}

void GameManager::createInstance(const unsigned int w,const unsigned int h)
{
	srand(time(NULL));
	instance = new GameManager(w,h);
	RenderManager::createInstance();
	ShaderManager::createInstance();	
	TextureManager::createInstance();
	CameraManager::createInstance();

	instance->initData();	
	
}


void GameManager::initData()
{

	ShaderManager::getInstance()->initShaderProgram("Simple","simple.vert","simple.frag","simple.vert","simple.frag");
	ShaderManager::getInstance()->initShaderProgram("StrategicMap","BasicStratMap.vert","basicStratMap.frag","BasicStratMap.vert","basicStratMap.frag");
	ShaderManager::getInstance()->initShaderProgram("GUI","GUI.vert","GUI.frag","GUI.vert","GUI.frag");

	TextureManager::getInstance()->load("Red","red.bmp",BMP);
	TextureManager::getInstance()->load("Blue","blue.bmp",BMP);
	TextureManager::getInstance()->load("Green","green.bmp",BMP);
	TextureManager::getInstance()->load("Fabric","fabric.bmp",BMP);
	TextureManager::getInstance()->load("Grass","grass.bmp",BMP);
	TextureManager::getInstance()->load("Water","water.bmp",BMP);
	TextureManager::getInstance()->load("Dirt","dirt.bmp",BMP);
	TextureManager::getInstance()->load("Stone","stone.bmp",BMP);
	TextureManager::getInstance()->load("Tree","tree.bmp",BMP);
	TextureManager::getInstance()->load("TileOverlay","tileOverlay.bmp",BMP);
	TextureManager::getInstance()->load("Play","Play.bmp",BMP);
	TextureManager::getInstance()->load("PlayDown","PlayDown.bmp",BMP);
	TextureManager::getInstance()->load("TacticalModeButton","TacticalButton.bmp",BMP);
	TextureManager::getInstance()->load("StrategicModeButton","StrategicButton.bmp",BMP);
	TextureManager::getInstance()->load("MainMenuButton","MainMenuButton.bmp",BMP);
	TextureManager::getInstance()->load("Windowbackdrop","WindowBack.bmp",BMP);
	TextureManager::getInstance()->load("WindowQuitDefault","windowQuitDefault.bmp",BMP);
	TextureManager::getInstance()->load("WindowDockDefault","windowDockDefault.bmp",BMP);
	TextureManager::getInstance()->load("WindowUndockDefault","windowMaximizeDefault.bmp",BMP);
	TextureManager::getInstance()->load("MainMenuButton","MainMenuButton.bmp",BMP);
	TextureManager::getInstance()->load("CheckboxUnchecked","checkboxUnchecked.bmp",BMP);
	TextureManager::getInstance()->load("CheckboxChecked","checkboxChecked.bmp",BMP);
	TextureManager::getInstance()->load("ContinentsButton","ContinentsButton.bmp",BMP);
	TextureManager::getInstance()->load("MediterreanButton","MediterreanButton.bmp",BMP);
	TextureManager::getInstance()->load("LargeIslandsButton","LargeIslandsButton.bmp",BMP);
	TextureManager::getInstance()->load("LandmassButton","LandmassButton.bmp",BMP);
	TextureManager::getInstance()->load("SmallIslandsButton","SmallIslandsButton.bmp",BMP);

	RenderManager::getInstance()->addRenderable("NULL",new Sprite("NULL"));
	RenderManager::getInstance()->addRenderable("Red",new Sprite("Red"));
	RenderManager::getInstance()->addRenderable("Blue",new Sprite("Blue"));
	RenderManager::getInstance()->addRenderable("Green",new Sprite("Green"));
	RenderManager::getInstance()->addRenderable("Fabric",new Sprite("Fabric"));
	RenderManager::getInstance()->addRenderable("GrassTile",new Sprite("Grass"));
	RenderManager::getInstance()->addRenderable("WaterTile",new Sprite("Water"));
	RenderManager::getInstance()->addRenderable("DirtTile",new Sprite("Dirt"));
	RenderManager::getInstance()->addRenderable("Resource-Stone",new Sprite("Stone"));
	RenderManager::getInstance()->addRenderable("Resource-Timber",new Sprite("Tree"));
	RenderManager::getInstance()->addRenderable("PoliticalTile",new Sprite("TileOverlay"));
	RenderManager::getInstance()->addRenderable("Play",new Sprite("Play"));
	RenderManager::getInstance()->addRenderable("PlayDown",new Sprite("PlayDown"));
	RenderManager::getInstance()->addRenderable("Windowbackdrop",new Sprite("Windowbackdrop"));
	RenderManager::getInstance()->addRenderable("WindowQuitDefault",new Sprite("WindowQuitDefault"));
	RenderManager::getInstance()->addRenderable("WindowDockDefault",new Sprite("WindowDockDefault"));
	RenderManager::getInstance()->addRenderable("WindowUndockDefault",new Sprite("WindowUndockDefault"));

	RenderManager::getInstance()->addRenderable("TacticalModeButton",new Sprite("TacticalModeButton"));
	RenderManager::getInstance()->addRenderable("StrategicModeButton",new Sprite("StrategicModeButton"));
	RenderManager::getInstance()->addRenderable("MainMenuButton",new Sprite("MainMenuButton"));

	RenderManager::getInstance()->addRenderable("CheckboxUnchecked",new Sprite("CheckboxUnchecked"));
	RenderManager::getInstance()->addRenderable("CheckboxChecked",new Sprite("CheckboxChecked"));


	RenderManager::getInstance()->addRenderable("ContinentsButton",new Sprite("ContinentsButton"));
	RenderManager::getInstance()->addRenderable("MediterreanButton",new Sprite("MediterreanButton"));
	RenderManager::getInstance()->addRenderable("LargeIslandsButton",new Sprite("LargeIslandsButton"));
	RenderManager::getInstance()->addRenderable("LandmassButton",new Sprite("LandmassButton"));
	RenderManager::getInstance()->addRenderable("SmallIslandsButton",new Sprite("SmallIslandsButton"));
	


	UnitData::load("Units.txt");
	

		
	stateId = previousState = MAIN_MENU;
	state = new MainMenuState();
}	


void GameManager::deleteInstance()
{
	delete instance;
	instance = NULL;
}

GameManager * GameManager::getInstance()
{
	if( instance == NULL) return NULL;

	return instance;
}


void GameManager::draw()
{
	state->draw();
}


void GameManager::update()
{
	state->update();
}
