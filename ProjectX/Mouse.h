#ifndef MOUSE_H
#define MOUSE_H
#include <SDL.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

enum MouseButtonState
{
	BUTTON_STATE_DOWN,
	BUTTON_STATE_UP
};


class Mouse
{

public:
	// standard Singleton function and setup
	static void createInstance();  
	static void deleteInstance();
	static Mouse * getInstance();

	// update view matrix then creates the inverse
	void updateViewMatrix(const glm::mat4 view){ invViewMatrix = glm::inverse(view);} 
	const glm::vec3 getRayOrigin(){return rayOrigin;}
	const glm::vec3 getRayDirection(){return rayDirection;}
	// function for calculating the ray, saves results into rayOrigin and rayDirection
	void calculateRay();

	void updateEvent(const SDL_Event e);
	const SDL_Event & getEvent() const { return currentEventState; }

	const MouseButtonState getRightState() const { return right; }
	const MouseButtonState getLeftState()  const { return left;  }
	const MouseButtonState getMiddleState()const { return middle;}

	const int getTimeStamp() const { return timeStamp; }
	const int getX() const { return x; }
	const int getY() const { return y; }
private:
	// holds the currentEventState so the class can access, variables from it and check it
	// if required
	SDL_Event currentEventState;
	int x,y;
	unsigned int timeStamp;
	
	MouseButtonState right,left,middle;
	
	
	// inverse matrices are needed to invert the 
	// matrices back to world space to compare with 3D objects etc.
	glm::mat4 invViewMatrix;
	glm::vec3 rayOrigin;
	glm::vec3 rayDirection;

	// private constructor/destructor as singletons do, static mouse instance to link to itself once
	// instantiated
	static Mouse * instance;
	Mouse();
	~Mouse();
};


#endif
