#include "TextureManager.h"

TextureManager * TextureManager::instance = NULL;

TextureManager::TextureManager()
{
}

void TextureManager::createInstance()
{
	if( !instance ) instance = new TextureManager();
	instance->load("NULL","null_texture.bmp",BMP);
}

TextureManager *TextureManager::getInstance()
{
	return instance;
}

void TextureManager::deleteInstance()
{
	delete instance;
}


TextureManager::~TextureManager()
{
	//glDeleteTextures(textures.size(),&textures[0]);
	textures.clear();
}

void TextureManager::load(const std::string name,std::string filePath,ImageType i)
{

	if( textures.find(name) != textures.end() ) return;

	TextureLoader *loader = TextureLoader::getLoader(i);

	if( loader == NULL ) 
	{
		std::cout << "Error with model loader, setting texture to default" << std::endl;
		textures[name] = textures["NULL"];
		return;
	}

	textures[name] = loader->load(filePath);

	delete loader;
}

void TextureManager::bind(const std::string name)
{
	std::map<std::string,Texture>::const_iterator itr = textures.find(name);

	if( itr != textures.end() )
	{
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D,itr->second.getHandle());
	} else 
	{
		std::cout << "Error: Could not find texture: " << name << std::endl;
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D,textures["NULL"].getHandle());
	}
}

const Texture& TextureManager::operator[](const std::string &name) const
{
	std::map<std::string,Texture>::const_iterator itr = textures.find(name);

	if( itr != textures.end() )
	{
		return itr->second;
	}

	//std::cout << "Error: No texture named " << name << std::endl;
	return textures.find("NULL")->second;
}

const int TextureManager::getHeight(const std::string & name) const 
{
	std::map<std::string,Texture>::const_iterator itr = textures.find(name);

	if( itr != textures.end() )
	{
		return itr->second.getHeight();
	}

	std::cout << "Error: No texture named " << name << std::endl;
	return textures.find("NULL")->second.getHeight();
}

const int TextureManager::getWidth(const std::string & name) const 
{
	std::map<std::string,Texture>::const_iterator itr = textures.find(name);

	if( itr != textures.end() )
	{
		return itr->second.getWidth();
	}

	std::cout << "Error: No texture named " << name << std::endl;
	return textures.find("NULL")->second.getWidth();
}





const float TextureManager::getWidthScreensapce(const std::string & name) const
{
	std::map<std::string,Texture>::const_iterator itr = textures.find(name);

	if( itr != textures.end() )
	{
		return itr->second.getWidthScreenspace();
	}

	std::cout << "Error: No texture named " << name << std::endl;
	return textures.find("NULL")->second.getWidthScreenspace();

}
const float TextureManager::getHeightScreensapce(const std::string & name) const
{
	std::map<std::string,Texture>::const_iterator itr = textures.find(name);

	if( itr != textures.end() )
	{
		return itr->second.getHeightScreenspace();
	}

	std::cout << "Error: No texture named " << name << std::endl;
	return textures.find("NULL")->second.getHeightScreenspace();
}