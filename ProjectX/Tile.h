#ifndef TILE_H
#define TILE_H


enum TileType
{
	WATER,
	GRASS,
	DIRT
};

class Tile
{
public:
	Tile() { }
	Tile(unsigned char tileType,unsigned char resource,unsigned char rAmount,unsigned char own) { type = tileType; resourceType = resource; resourceAmount = rAmount; owner = own; }
	

	inline void setOwner(unsigned char o) { owner = o; }
	inline void setType(unsigned char t){ type = t;}
	inline void setResourceAmount(unsigned char a){ resourceAmount = a;}
	inline void setResource(unsigned char r) { resourceType =r; }
	inline const TileType getType() const         {return (TileType)type;}
	inline const unsigned char getResourceType() const   {return resourceType;}
	inline const unsigned char getResourceAmount() const {return resourceAmount;}
	inline const unsigned char getOwner() const {return owner;}
private:
	unsigned char resourceType,resourceAmount;
	unsigned char type;
	unsigned char owner; //which faction owns this province
};

#endif