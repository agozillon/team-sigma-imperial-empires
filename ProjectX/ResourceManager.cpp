#include "ResourceManager.h"
#include "RenderManager.h"

ResourceManager * ResourceManager::instance = nullptr;


ResourceManager::ResourceManager()
{
	int x = 5;
}

ResourceManager::~ResourceManager()
{
}


void ResourceManager::createInstance()
{
	if( instance ) return;
	instance = new ResourceManager();
}

void ResourceManager::deleteInstance()
{
	if( !instance ) return;
	delete instance;
	instance = nullptr;
}


ResourceManager * ResourceManager::getInstance()
{
	if( !instance ) createInstance();
	return instance;
}


void ResourceManager::renderResource(const Tile * tile)
{
	renderResource((ResourceType)(tile->getResourceType()));
}

void ResourceManager::renderResource(const ResourceType r)
{
	if( r == NULL_RESOURCE ) return;

	if( r == TIMBER ) 
		RenderManager::getInstance()->renderRenderable("Resource-Timber");
	else if( r == STONE )
		RenderManager::getInstance()->renderRenderable("Resource-Stone");
}

void ResourceManager::renderResourceIcon(const ResourceType r)
{
}
