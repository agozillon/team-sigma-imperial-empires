#include "TacticalMapState.h"
#include "TacticalMapManager.h"
#include "CameraManager.h"

TacticalMapState::TacticalMapState()
{
	TacticalMapManager::createInstance();
}

TacticalMapState::~TacticalMapState()
{
	TacticalMapManager::deleteInstance();
}


void TacticalMapState::draw()
{	
	TacticalMapManager::getInstance()->draw();
}


void TacticalMapState::update()
{
	TacticalMapManager::getInstance()->update();
	checkInput();
}

void TacticalMapState::checkInput()
{
	TacticalMapManager::getInstance()->checkKeyPress();
}


void TacticalMapState::onEntry()
{
	// sets the active camera to the Tactical Map stops the camera being stuck on strategic
	CameraManager::getInstance()->setActiveCamera("TacticalMap");
}
