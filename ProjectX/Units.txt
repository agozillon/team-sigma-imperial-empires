//This file loads in all the various units
// use "//" to comment out lines 
//
// Name
// {
//     Type attibueName value  //note the lack of an equals sign
// }
// where type is "String","Float","Integer" or "Boolean"
// For Boolean false == 0 and true == any non zero number


NULL
{
	String ImageName NULL
	String ModelName NULL
}

Regular
{
	Integer Health 100
	Float damage 20.0
	Boolean test 0

	//Where image name is the renderable used in the strategic map
	//and the ModelName is the renderable used in the tactical mode
	String ImageName GrassTile
	String ModelName NULL
}


//automaticly removes the underscore
Coldstream_Guard
{
	Integer Health 200
	Float damage 50.0

	String ImageName DirtTile
	String ModelName NULL
}

