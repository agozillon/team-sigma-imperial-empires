#ifndef MAP_MANAGER_H
#define MAP_MANAGER_H
#include "Tile.h"
#include "RenderManager.h"


enum MapMode
{
	TERRAIN_MAP,
	POLITICAL_MAP,
	RESOURCE_MAP
};

class MapManager
{
public:
	static void createInstance();
	static void deleteInstance();
	static MapManager* getInstance();


	void draw(const Tile*,glm::mat4& );
	void switchMode(const MapMode);
	

private:
	
	void checkCollision();


	int active;
	MapManager();
	~MapManager();
	static MapManager * instance;
	MapMode mode;

};

#endif