#ifndef FORESTLEVEL_H
#define FORESTLEVEL_H
#include "Scenery.h"
#include "Level.h"
#include <vector>

class ForestLevel : public Level
{
public:
	ForestLevel();
	~ForestLevel();
	void init();
	void draw();
	void update();
	void getScenery(std::vector<Scenery*> &scenery);

private:
	std::vector<Scenery*> sceneryList;

};
#endif