#ifndef WORLDOBJECTS_H
#define WORLDOBJECTS_H
#include <glm/gtc/type_ptr.hpp> // including glm/gtc/type_ptr so that this class can use vec3s 
#include "BoundingBox.h"
using namespace glm;	

class WorldObjects
{
public:
	virtual const vec3 getPosition() = 0;
	virtual const vec3 getRotation() = 0;
	virtual const vec3 getScalar() = 0;
	virtual void updatePosition(vec3 pos) = 0;
	virtual void updateRotation(vec3 rot) = 0;
	virtual void updateScalar(vec3 scale) = 0;
	virtual void draw(mat4 viewProjection) = 0;
	virtual const BoundingBox * getCollisionBox() = 0;
	virtual ~WorldObjects(){}

private:
	vec3 scalar;
	vec3 rotation;
	vec3 position;
	std::string meshName;
	std::string textureName;
	std::string shaderName;
	BoundingBox * collisionBox;

};

#endif