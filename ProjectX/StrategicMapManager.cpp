#include "StrategicMapManager.h"
#include <iostream>
#include "TileLoader.h"

#include <glm\glm.hpp>
#include <glm\gtc\matrix_transform.hpp>
#include <glm\gtc\type_ptr.hpp>

#include "CameraManager.h"
#include <SDL.h>
#include "MapManager.h"
#include "ResourceManager.h"
#include "FactionManager.h"
#include "GameManager.h"
#include "MapGenerator.h"
#include "WindowWidget.h"
#include "Button.h"

StrategicMapManager* StrategicMapManager::instance = NULL;

void StrategicMapManager::createInstance()
{
	if( instance ) return;

	instance = new StrategicMapManager();
}


void StrategicMapManager::deleteInstance()
{

	if( !instance) return;

	delete instance;
	instance = NULL;
}

StrategicMapManager* StrategicMapManager::getInstance()
{
	if( !instance ) createInstance();

	return instance;
}


void StrategicMapManager::addGenerationGUI()
{

	if( widgetManager.getWidget("Window") != NULL) return;
	
	WindowWidget * window = new WindowWidget("Window",(const Sprite*)RenderManager::getInstance()->getRenderable("Windowbackdrop"),0.0,0.0,WIDGET_DRAGABLE | WIDGET_PERFORM_CHILD_CLEANUP, WINDOW_WIDGET_CLOSABLE);
	
	
	window->scale(3,3);
	
	Button *button = new Button("Mediterranean",(const Sprite*)RenderManager::getInstance()->getRenderable("MediterreanButton"),0.10f,0.2f,WIDGET_NULL,BUTTON_SHADER_TINT);
	button->scale(2,2);	
	button->addEventHandler(widgetManager.getEventHandler());
	window->addWidget(button);

	button = new Button("SmallIslands",(const Sprite*)RenderManager::getInstance()->getRenderable("SmallIslandsButton"),0.1f,0.85f,WIDGET_NULL,BUTTON_SHADER_TINT);
	button->scale(2,2);
	window->addWidget(button);
	button->addEventHandler(widgetManager.getEventHandler());

	button = new Button("LargeIslands",(const Sprite*)RenderManager::getInstance()->getRenderable("LargeIslandsButton"),0.1f,0.55f,WIDGET_NULL,BUTTON_SHADER_TINT);
	button->scale(2,2);
	window->addWidget(button);
	button->addEventHandler(widgetManager.getEventHandler());
	
	button = new Button("Continents",(const Sprite*)RenderManager::getInstance()->getRenderable("ContinentsButton"),0.5f,0.3f,WIDGET_NULL,BUTTON_SHADER_TINT);
	button->scale(2,2);
	window->addWidget(button);
	button->addEventHandler(widgetManager.getEventHandler());
	
	button = new Button("Landmass",(const Sprite*)RenderManager::getInstance()->getRenderable("LandmassButton"),0.5f,0.55f,WIDGET_NULL,BUTTON_SHADER_TINT);
	button->scale(2,2);
	window->addWidget(button);
	button->addEventHandler(widgetManager.getEventHandler());

	widgetManager.addWidget(window);
}

StrategicMapManager::StrategicMapManager()
{

	StrategicModeWidgetEventHandler * handler = new StrategicModeWidgetEventHandler();
	widgetManager.addEventHandler(handler);
	addGenerationGUI();


	CameraManager::getInstance()->addCamera("SM_Default",glm::vec3(5.0f,5.0f,5.0f),glm::vec3(0.0f,0.0f,0.00000000001f),glm::vec3(0.0f,0.0f,1.0f));
	CameraManager::getInstance()->setActiveCamera("SM_Default");


	ResourceManager::createInstance();
	MapManager::createInstance();

	FactionManager::createInstance();
	FactionManager::getInstance()->add(new Faction("British Empire",1,glm::vec4(1.0f,0.0f,0.0f,1.0f)));
	FactionManager::getInstance()->add(new Faction("France",2,glm::vec4(16/100.0f,131.0f/100.0f,255.0f/100.0f,1.0f)));

	FactionManager::getInstance()->add(new Faction("Russia",3,glm::vec4(11.0f/255.0f,94.0f/255.0f,5.0f/255.0f,1.0f)));	
	FactionManager::getInstance()->add(new Faction("Prussia",4,glm::vec4(0.0f,0.0f,102.0f/255.0f,1.0f)));
	FactionManager::getInstance()->add(new Faction("Netherlands",5,glm::vec4(1.0f,128.0f/255.0f,0.0f,1.0f)));
	
	map = TileLoader::load("Map.txt");
}

StrategicMapManager::~StrategicMapManager()
{
	FactionManager::deleteInstance();
	ResourceManager::deleteInstance();
	MapManager::deleteInstance();

	delete map;
}


void StrategicMapManager::draw()
{
	map->draw();
	widgetManager.draw();
}

void StrategicMapManager::update()
{

}


void StrategicMapManager::checkKeyPress()
{
	map->checkMouseCollision();
	widgetManager.checkMouse();


	Uint8 * keys = SDL_GetKeyboardState(NULL);

	if( keys[SDL_SCANCODE_W] ) CameraManager::getInstance()->setEye( CameraManager::getInstance()->getEye() + glm::vec3(0.0f,0.5f,0.0f) );
	if( keys[SDL_SCANCODE_S] ) CameraManager::getInstance()->setEye( CameraManager::getInstance()->getEye() + glm::vec3(0.0f,-0.5f,0.0f) );
	
	if( keys[SDL_SCANCODE_A] ) CameraManager::getInstance()->setEye( CameraManager::getInstance()->getEye() + glm::vec3(-0.5f,0.0f,0.0f) );
	if( keys[SDL_SCANCODE_D] ) CameraManager::getInstance()->setEye( CameraManager::getInstance()->getEye() + glm::vec3(0.5f,0.0f,0.0f) );

	if( keys[SDL_SCANCODE_Z] ) CameraManager::getInstance()->setEye( CameraManager::getInstance()->getEye() + glm::vec3(0.0f,0.0f,-0.5f) );
	if( keys[SDL_SCANCODE_X] ) CameraManager::getInstance()->setEye( CameraManager::getInstance()->getEye() + glm::vec3(0.0f,0.0f,0.5f) );


	if( keys[SDL_SCANCODE_1] ) MapManager::getInstance()->switchMode(TERRAIN_MAP);
	if( keys[SDL_SCANCODE_2] ) MapManager::getInstance()->switchMode(POLITICAL_MAP);
	if( keys[SDL_SCANCODE_3] ) MapManager::getInstance()->switchMode(RESOURCE_MAP);

	if( keys[SDL_SCANCODE_ESCAPE]) GameManager::getInstance()->changeState(MAIN_MENU);


	if( keys[SDL_SCANCODE_SPACE] ) addGenerationGUI();
}



void StrategicMapManager::generateNewMap(MapType type)
{
	if( map ) delete map;

	MapGenerator mapGen;
	map = mapGen.generateMap(100,100,3,type);

}