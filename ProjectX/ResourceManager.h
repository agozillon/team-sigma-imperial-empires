#ifndef RESOURCE_MANAGER_H
#define RESOURCE_MANAGER_H
#include "Tile.h"

enum ResourceType
{
    NULL_RESOURCE,
	TIMBER,
	STONE,
	IRON,
	COAL
};


class ResourceManager
{
public:

	static ResourceManager * getInstance();
	static void createInstance();
	static void deleteInstance();

	void renderResource(const Tile*);
	void renderResource(const ResourceType);

	void renderResourceIcon(const ResourceType);
private:
	static ResourceManager * instance;
	ResourceManager();
	~ResourceManager();

};

#endif